"""Unit tests covering starfort.ui.windowing.layout"""

from unittest import mock

from nose.tools import eq_

from starfort.ui import layout, Axis, Alignment


def _test_layout(instance, expected_width, expected_height, draw_calls=None, draw_x=0, draw_y=0):
    draw_calls = draw_calls or []
    eq_(instance.width, expected_width)
    eq_(instance.height, expected_height)

    for method, _ in draw_calls:
        eq_(method.call_count, 0)

    instance.draw(x=draw_x, y=draw_y)

    for method, kwargs in draw_calls:
        eq_(method.call_count, 1)
        eq_(method.call_args.kwargs, kwargs)
        method.reset_mock()


def test_stack():
    a = mock.MagicMock(width=100, height=800)
    b = mock.MagicMock(width=300, height=110)

    yield _test_layout, layout.Stack(), 0, 0

    yield _test_layout, layout.Stack(
        a,
        h_align=Alignment.START,
        v_align=Alignment.START
    ), 100, 800, [
        (a.draw, dict(x=20, y=30)),
    ], 20, 30

    yield _test_layout, layout.Stack(
        a, b,
        h_align=Alignment.START,
        v_align=Alignment.START
    ), 300, 800, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=20, y=30))
    ], 20, 30

    yield _test_layout, layout.Stack(
        a, b,
        h_align=Alignment.CENTER,
        v_align=Alignment.CENTER
    ), 300, 800, [
        (a.draw, dict(x=120, y=30)),
        (b.draw, dict(x=20, y=375))
    ], 20, 30


def test_align():
    a = mock.MagicMock(width=100, height=800)
    b = mock.MagicMock(width=300, height=110)
    c = mock.MagicMock(width=50, height=50)

    yield _test_layout, layout.Align(axis=Axis.HORIZONTAL), 0, 0

    yield _test_layout, layout.Align(axis=Axis.VERTICAL), 0, 0

    yield _test_layout, layout.Align(
        a,
        axis=Axis.HORIZONTAL,
        align=Alignment.START
    ), 100, 800, [
        (a.draw, dict(x=20, y=30))
    ], 20, 30

    yield _test_layout, layout.Align(
        a, b, c,
        axis=Axis.HORIZONTAL,
        align=Alignment.START
    ), 450, 800, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=120, y=30)),
        (c.draw, dict(x=420, y=30))
    ], 20, 30

    yield _test_layout, layout.Align(
        a, b, c,
        axis=Axis.HORIZONTAL,
        align=Alignment.CENTER
    ), 450, 800, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=120, y=375)),
        (c.draw, dict(x=420, y=405))
    ], 20, 30

    yield _test_layout, layout.Align(
        a, b, c,
        axis=Axis.VERTICAL,
        align=Alignment.CENTER
    ), 300, 960, [
        (a.draw, dict(x=120, y=30)),
        (b.draw, dict(x=20, y=830)),
        (c.draw, dict(x=145, y=940))
    ], 20, 30


def test_fixedalign():
    a = mock.MagicMock(width=100, height=800)
    b = mock.MagicMock(width=300, height=110)
    c = mock.MagicMock(width=50, height=50)

    yield _test_layout, layout.FixedAlign(
        a, b, c,
        axis=Axis.HORIZONTAL,
        align=Alignment.CENTER,
        space=100
    ), 400, 800, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=120, y=375)),
        (c.draw, dict(x=220, y=405))
    ], 20, 30

    yield _test_layout, layout.FixedAlign(
        a, b, c,
        axis=Axis.VERTICAL,
        align=Alignment.CENTER,
        space=100
    ), 300, 800, [
        (a.draw, dict(x=120, y=30)),
        (b.draw, dict(x=20, y=130)),
        (c.draw, dict(x=145, y=230))
    ], 20, 30


def test_grid():
    a = mock.MagicMock(width=8, height=9)
    b = mock.MagicMock(width=6, height=0)
    c = mock.MagicMock(width=7, height=3)
    d = mock.MagicMock(width=5, height=5)
    e = mock.MagicMock(width=3, height=7)
    f = mock.MagicMock(width=0, height=6)
    g = mock.MagicMock(width=9, height=8)

    # a b c
    # d e f
    # g _ _
    yield _test_layout, layout.Grid(
        a, b, c, d, e, f, g,
        axis=Axis.HORIZONTAL,
        h_align=Alignment.START,
        v_align=Alignment.START,
        line_limit=3
    ), 22, 24, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=29, y=30)),
        (c.draw, dict(x=35, y=30)),
        (d.draw, dict(x=20, y=39)),
        (e.draw, dict(x=29, y=39)),
        (f.draw, dict(x=35, y=39)),
        (g.draw, dict(x=20, y=46))
    ], 20, 30

    # a d g
    # b e _
    # c f _
    yield _test_layout, layout.Grid(
        a, b, c, d, e, f, g,
        axis=Axis.VERTICAL,
        h_align=Alignment.START,
        v_align=Alignment.START,
        line_limit=3
    ), 22, 22, [
        (a.draw, dict(x=20, y=30)),
        (b.draw, dict(x=20, y=39)),
        (c.draw, dict(x=20, y=46)),
        (d.draw, dict(x=28, y=30)),
        (e.draw, dict(x=28, y=39)),
        (f.draw, dict(x=28, y=46)),
        (g.draw, dict(x=33, y=30))
    ], 20, 30
