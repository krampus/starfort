"""Starfort (name TBD) -- A space-station management simulator"""

from setuptools import setup, find_packages, Extension
import numpy

requires = [
    'pyglet>=1.5.5',
    'numpy>=1.18.4',
]

extras = {
    'logging': [
        'ansicolors',
    ],
    'testing': [
        'coverage',
        'nose',
        'rednose',
    ],
    'debug': [
        'psutil',
    ],
    'develop': [
        'ansicolors',
        'autopep8',
        'bpython',
        'flake8',
        'jedi',
        'snakeviz',
        'yapf',
    ]
}

extras['complete'] = list({pkg for req in extras.values() for pkg in req})

setup(
    name='starfort',
    use_scm_version=True,
    author='Rob Kelly',
    author_email='contact@robkel.ly',
    description=__doc__,
    url=None,  # TODO
    entry_points={
        'console_scripts': [
            'starfort = starfort.__main__:main',
        ],
    },
    ext_modules=[
        Extension('starfort.game.extensions', sources=['starfort/game/extensions.pyx']),
    ],
    include_dirs=[numpy.get_include()],
    packages=find_packages(include=['starfort*']),
    include_package_data=True,
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Games/Entertainment :: Simulation",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.8',
    setup_requires=[
        'setuptools_scm',
        'Cython'
    ],
    install_requires=requires,
    extras_require=extras,
)
