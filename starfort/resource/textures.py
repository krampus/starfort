"""Graphical data access"""
import sys

import pyglet

from . import memoize

# All textures should use nearest-neighbor filtering
# TODO fix sprite bleed-over at certain magnifications
# pyglet.image.Texture.default_min_filter = pyglet.gl.GL_NEAREST
pyglet.image.Texture.default_mag_filter = pyglet.gl.GL_NEAREST

_ANCHOR_X = 0
_ANCHOR_Y = 16


def load_sheet(path: str, w: int, h: int,
               anchor_x: int = _ANCHOR_X, anchor_y: int = _ANCHOR_Y) -> pyglet.image.TextureGrid:
    pyglet.resource.reindex()
    img = pyglet.resource.image(path)
    seq = pyglet.image.ImageGrid(img, w, h)
    tex = pyglet.image.TextureGrid(seq)

    for t in tex:
        t.anchor_x, t.anchor_y = anchor_x, anchor_y

    return tex


class TextureAtlas:
    TILE_DIM = 64
    DEFAULT_ANCHOR_X = _ANCHOR_X
    DEFAULT_ANCHOR_Y = _ANCHOR_Y

    @property
    @memoize
    def TILESHEET(self) -> pyglet.image.TextureGrid:
        return load_sheet('image/tilesheet.png', 16, 16)

    @property
    @memoize
    def ENTITYSHEET(self) -> pyglet.image.TextureGrid:
        return load_sheet('image/entitysheet.png', 16, 16)

    @property
    @memoize
    def UI(self) -> pyglet.image.TextureGrid:
        return load_sheet('image/ui.png', 16, 16, anchor_x=0, anchor_y=0)


sys.modules[__name__] = TextureAtlas()
