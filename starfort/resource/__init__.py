"""Game resource IO and access"""
from functools import wraps
from pathlib import Path
import logging

import pyglet

logger = logging.getLogger(__name__)

datapath = Path(__file__).absolute().parent / 'data'
pyglet.resource.path = [str(datapath)]
pyglet.resource.reindex()


def memoize(fn):
    """Quick memoizing decorator for zero-argument methods"""
    key = f"_memo_{fn.__name__}"

    @wraps(fn)
    def wrapped(self):
        if not hasattr(self, key):
            setattr(self, key, fn(self))
            logger.debug(f"Memoized {fn.__name__}")
        return getattr(self, key)

    return wrapped
