"""Font loading resources"""

import pyglet

# These are just a "palette" of useful fonts
DEBUG = "Arial"
MONITOR = "League Gothic"
TECH = "Orbitron"


def load():
    # pyglet doesn't know how to handle most font properties, so we'll help it
    font_cache = pyglet.font._font_class._memory_faces._dict

    pyglet.resource.add_font("font/league-gothic/LeagueGothic-CondensedRegular.otf")
    font_cache[('league gothic condensed', False, False)] = font_cache.pop(('league gothic', False, False))

    pyglet.resource.add_font("font/league-gothic/LeagueGothic-CondensedItalic.otf")
    font_cache[('league gothic condensed', False, True)] = font_cache.pop(('league gothic', False, False))

    pyglet.resource.add_font("font/league-gothic/LeagueGothic-Italic.otf")
    font_cache[('league gothic', False, True)] = font_cache.pop(('league gothic', False, False))

    pyglet.resource.add_font("font/league-gothic/LeagueGothic-Regular.otf")

    pyglet.resource.add_font("font/orbitron/Orbitron Light.otf")
    font_cache[('orbitron light', False, False)] = font_cache.pop(('orbitron', False, False))

    pyglet.resource.add_font("font/orbitron/Orbitron Black.otf")
    font_cache[('orbitron black', False, False)] = font_cache.pop(('orbitron', False, False))

    pyglet.resource.add_font("font/orbitron/Orbitron Medium.otf")
    pyglet.resource.add_font("font/orbitron/Orbitron Bold.otf")
