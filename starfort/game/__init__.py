"""Game logic."""
from __future__ import annotations

import typing as T

TILE_W = 64
TILE_H = 32


def grid_to_screen(x: float, y: float) -> T.Tuple[float, float]:
    """Translate coordinates from grid-space to virtual screen-space"""
    return TILE_W / 2 * (y + x), TILE_H / 2 * (y - x)


def screen_to_grid(x: float, y: float) -> T.Tuple[float, float]:
    """Translate coordinates from virtual screen-space to grid-space"""
    return (TILE_H * x - TILE_W * y) / (TILE_W * TILE_H), (TILE_H * x + TILE_W * y) / (TILE_W * TILE_H)


def event_property(base_name: str, event_name: str = None, docstring: str = None):
    """Shortcut for declaring properties that fire an event on change."""
    event_name = event_name or f"on_set_{base_name.lstrip('_')}"

    def getter(self):
        return getattr(self, base_name)

    def setter(self, value):
        setattr(self, base_name, value)
        self.dispatch_event(event_name, value)

    return property(getter, setter, doc=docstring)


def enum_dispatch(method_name: str):
    """Dynamic dispatch for enum members.

    Calls to the returned method will be dispatched to the instance
    method named "{method_name}_{member name}", if it exists. If it
    does not exist, the call is dispatched to the method named
    "{method_name}_default" instead. If that default method doesn't
    exist either, the call returns ``None``.
    """
    def dispatcher(self, *args, **kwargs):
        key = f"{method_name}_{self.name}"
        if hasattr(self, key):
            return getattr(self, key)(*args, **kwargs)
        elif hasattr(self, f"{method_name}_default"):
            return getattr(self, f"{method_name}_default")(*args, **kwargs)


class NamedMixin:
    """Mixin for classes whose instances should have a user-readable name."""

    @property
    def display_name(self) -> str:
        """User-readable name for this instance."""
        return self.__class__.__name__
