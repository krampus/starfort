"""Modelling & simulation utilities."""
from __future__ import annotations
import typing as T
import string
from dataclasses import dataclass

import numpy as np


def _make_rv(rng_method: T.Callable,
             doc: T.Optional[str] = None,
             **attrs: T.Union[T.Type, T.Tuple[T.Type, T.Any]]) -> T.Type[AbstractRV]:
    def sample(self, size: T.Optional[T.Tuple] = None):
        return rng_method(self._rng, *(getattr(self, arg) for arg in attrs), size=size)

    annotations = {}
    defaults = {}
    for k, v in attrs.items():
        if isinstance(v, T.Tuple):
            annotations[k], defaults[k] = v
        else:
            annotations[k] = v

    return dataclass(type(
        string.capwords(rng_method.__name__, sep='_').replace('_', '') + "RV",
        (AbstractRV,),
        {
            '__doc__': doc,
            '__annotations__': annotations,
            'sample': sample,
            **defaults,
        }
    ))


class AbstractRV:
    """Abstract random variable."""

    @staticmethod
    def seed(seed: T.Union[None, int, T.Iterable[int],
                           np.random.SeedSequence, np.random.BitGenerator,
                           np.random.Generator] = None):
        AbstractRV._rng = np.random.default_rng(seed)

    def _entropy_like(self, size: T.Optional[T.Tuple]):
        if size is None:
            return self._rng.random()
        else:
            return self._rng.random(size)

    def sample(self, size: T.Optional[T.Tuple] = None):
        """Sample this random variable."""
        raise NotImplementedError("Implemented by derived type")


@dataclass
class MappedRV(AbstractRV):
    """A transformation applied to a random variable.

    In almost all cases, this will be substantially slower than using
    an ordinary instance of AbstractRV.
    """
    base: AbstractRV
    func: T.Callable[[float], float]

    def sample(self, size: T.Optional[T.Tuple] = None):
        return self.func(self.base.sample(size))


@dataclass
class ClippedRV(AbstractRV):
    """Random variable, clipped at either a minimum, maximum, or both."""
    base: AbstractRV
    minimum: T.Optional[float] = None
    maximum: T.Optional[float] = None

    def sample(self, size: T.Optional[T.Tuple] = None):
        return np.clip(self.base.sample(size), self.minimum, self.maximum)


# XXX this can potentially be replaced with scipy.stats.rv_discrete
# if we want to add a huge dependency
@dataclass
class DiscreteRV(AbstractRV):
    """Discrete random variable defined by an arbitrary inverse CDF."""
    cdf: np.ndarray

    def __post_init__(self):
        # REMOVEME
        self._sanity_check()

    def _sanity_check(self):
        """Check that the underlying inverse CDF is valid.

        Useful for debugging.
        """
        if self.cdf[0, 0] != 0:
            raise ValueError("Discrete CDF must define a mapping for 0.")
        for (x1, y1), (x2, y2) in zip(self.cdf, self.cdf[1:]):
            if not 0 <= x1 < x2 < 1:
                raise ValueError("Domain values of discrete CDF must be strictly increasing on [0, 1).")
            if not y1 < y2:
                raise ValueError("Range of discrete CDF must be strictly increasing.")

    def sample(self, size: T.Optional[T.Tuple] = None):
        entropy = self._entropy_like(size)

        reshape = (-1,) + tuple(1 for _ in size)
        idx = (self.cdf[:, 0].reshape(reshape) < entropy).argmin(axis=0) - 1
        return self.cdf[idx, 1]


BetaRV = _make_rv(np.random.Generator.beta, a=float, b=float,
                  doc="Beta-distributed random variable.")
BinomialRV = _make_rv(np.random.Generator.binomial, n=int, p=float,
                      doc="Binomial-distributed random variable.")
ChisquareRV = _make_rv(np.random.Generator.chisquare, df=float,
                       doc="Chi-square distributed random variable.")
ExponentialRV = _make_rv(np.random.Generator.exponential, scale=float,
                         doc="Exponential-distributed random variable.")
GammaRV = _make_rv(np.random.Generator.gamma, shape=float, scale=(float, 1.),
                   doc="Gamma-distributed random variable.")
GeometricRV = _make_rv(np.random.Generator.geometric, p=float,
                       doc="Geometric-distributed random variable.")
LogisticRV = _make_rv(np.random.Generator.logistic, loc=(float, 0.), scale=(float, 1.),
                      doc="Logistic-distributed random variable.")
LognormalRV = _make_rv(np.random.Generator.lognormal, mean=(float, 0.), sigma=(float, 1.),
                       doc="Log-normal distributed random variable.")
NegativeBinomialRV = _make_rv(np.random.Generator.negative_binomial, n=float, p=float,
                              doc="Negative-binomial distributed random variable.")
NormalRV = _make_rv(np.random.Generator.normal, loc=(float, 0.), scale=(float, 1.),
                    doc="Normal (Gaussian) distributed random variable.")
PoissonRV = _make_rv(np.random.Generator.poisson, lam=(float, 1.),
                     doc="Poisson-distributed random variable.")
StandardNormalRV = _make_rv(np.random.Generator.standard_normal,
                            doc="Standard normal (Gaussian) distributed random variable.")
WeibullRV = _make_rv(np.random.Generator.weibull, a=float,
                     doc="Weibull-distributed random variable.")
UniformRV = _make_rv(np.random.Generator.uniform, low=(float, 0.), high=(float, 1.),
                     doc="Uniformly-distributed random variable.")
ZipfRV = _make_rv(np.random.Generator.zipf, a=float,
                  doc="Zipf-distributed random variable.")


class BernoulliRV(BinomialRV):
    "Bernoulli-distributed random variable."
    def __init__(self, p: float):
        super().__init__(n=1., p=p)


# initially seed the RNG with entropy from the OS
AbstractRV.seed()
