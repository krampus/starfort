"""Cython extensions used by starfort"""

import numpy as np
cimport numpy as np
cimport cython

from libc.stdint cimport uint8_t

ctypedef np.float32_t heat_t

# cdef struct RGBAColor:
#     np.uint8_t r
#     np.uint8_t g
#     np.uint8_t b
#     np.uint8_t a

ctypedef (uint8_t, uint8_t, uint8_t, uint8_t) RGBAColor

@cython.boundscheck(False)
@cython.wraparound(False)
def simulate_heatmap(np.ndarray[heat_t, ndim=3] heatmap,
                     np.ndarray[np.uint8_t, ndim=3, cast=True] mask,
                     float rate = 0.5,
                     float padding = 0.0) -> np.ndarray:
    """Run one step of simulated heat decay on a heatmap.

    This implementation ignores boundary cases, namely, the first and
    last elements on each axis. This is done deliberately. Solutions
    include manually padding the input heatmap and mask, or just
    ignoring it.
    """
    cdef np.uint8_t [:, :, :] inv_mask = np.uint8(1) - mask
    cdef np.ndarray[heat_t, ndim=3] means = np.full_like(heatmap, padding)
    cdef heat_t [:, :, :] heatmap_view = heatmap
    cdef heat_t [:, :, :] means_view = means

    cdef np.uint8_t n_unmasked

    cdef Py_ssize_t z, x, y

    for z in range(1, heatmap.shape[0] - 1):
        for x in range(1, heatmap.shape[1] - 1):
            for y in range(1, heatmap.shape[2] - 1):
                if inv_mask[z, x, y]:
                    n_unmasked = (
                        1 +
                        inv_mask[z - 1, x, y] +
                        inv_mask[z + 1, x, y] +
                        inv_mask[z, x - 1, y] +
                        inv_mask[z, x + 1, y] +
                        inv_mask[z, x, y - 1] +
                        inv_mask[z, x, y + 1]
                    )
                    if n_unmasked > 0:
                        means_view[z, x, y] = (
                            heatmap_view[z, x, y] +
                            heatmap_view[z - 1, x, y] * inv_mask[z - 1, x, y] +
                            heatmap_view[z + 1, x, y] * inv_mask[z + 1, x, y] +
                            heatmap_view[z, x - 1, y] * inv_mask[z, x - 1, y] +
                            heatmap_view[z, x + 1, y] * inv_mask[z, x + 1, y] +
                            heatmap_view[z, x, y - 1] * inv_mask[z, x, y - 1] +
                            heatmap_view[z, x, y + 1] * inv_mask[z, x, y + 1]
                        ) / n_unmasked
                    else:
                        means_view[z, x, y] = padding

    return heatmap * (1 - rate) + means * rate
