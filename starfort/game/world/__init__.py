"""Game world"""
from __future__ import annotations

import typing as T
import queue
from dataclasses import dataclass
from enum import Enum
import functools
import logging

import numpy as np
import pyglet

from . import tile, render
from ... import eventloop
from ...exceptions import GameLogicException
from .. import event_property, extensions
from ..entities import Entity, items


logger = logging.getLogger(__name__)

_TIMESTEP_STOPPED = -1
_TIMESTEP_FREERUN = 0

# number of steps between atmosphere updates
_ATMOSPHERE_UPDATE_STEPS = 4
_ATMOSPHERE_DISPERSION_RATE = 1.0


class Timescale(Enum):
    """Adjustable scaling for the period of world steps, in milliseconds."""
    FREERUN = 0
    FAST = 100
    MEDIUM = 300
    SLOW = 1000


class Designation(Enum):
    """Designations for world elements."""
    NONE = 0
    FORBID = 1
    PRIORITY = 2
    DESTROY = 3


@dataclass
class Path:
    world: World
    path: np.ndarray

    def __len__(self) -> int:
        return self.path.shape[1]

    def pop(self) -> T.Tuple[int, int, int]:
        item = tuple(self.path[:, 0])
        self.path = self.path[:, 1:]
        return item


def with_default_location(func: T.Callable) -> T.Callable:
    """Wraps a World method with handling for (z, x, y) argument defaults.

    The default behavior is to use the active layer as `z` if not
    given, and to use the cursor location for `x` and `y` if either is
    not given. If `x` or `y` is not given and the cursor is not set,
    the wrapper will return ``None`` without calling the wrapped
    method.
    """
    @functools.wraps(func)
    def wrapped(self,
                z: T.Optional[int] = None,
                x: T.Optional[int] = None,
                y: T.Optional[int] = None,
                **kwargs):
        z = self.active_layer if z is None else z
        if x is None or y is None:
            if self.cursor is None:
                return None
            else:
                x, y = self.cursor
        return func(self, z, x, y, **kwargs)
    return wrapped


class World(pyglet.event.EventDispatcher):
    """Model of the game world grid."""
    def __init__(self, shape: T.Tuple[int, int, int], timescale: Timescale = Timescale.MEDIUM):
        super().__init__()
        for event in ['on_set_pause_state', 'on_set_timescale',
                      'on_set_cursor', 'on_set_active_layer', 'on_atmosphere_update',
                      'on_set_atmosphere_overlay_display']:
            self.register_event_type(event)

        # minimum milliseconds per step
        self.timescale = timescale
        self._ms_since_last_step = 0
        self.paused = True
        self.cursor = None

        # world tile grid (z, x, y)
        self.tiles = np.zeros(shape, dtype=np.uint8)

        # Atmospheric pressure & related
        self.atmosphere = np.zeros(shape, dtype=np.float32)
        self._atmosphere_update_future = None
        self._atmosphere_substeps = 0

        quad_grid = render.quad_grid(shape[1:])
        n_verts = shape[1] * shape[2] * 4

        self._atmosphere_vlist = pyglet.graphics.vertex_list(
            n_verts,
            ('v2f', quad_grid.reshape(-1)),
            ('c4B', (255, 0, 0, 100) * n_verts)
        )
        self.draw_atmosphere_overlay = False

        # Designations
        self.designations = np.zeros(shape, dtype=np.uint8)
        self._designation_layers = [pyglet.graphics.Batch() for _ in self.tiles]

        self._entities = []
        self._entities_added = []

        RenderLayer = render.layer_factory(shape[1:])
        self._render_tree = [RenderLayer(self.tiles[i], self.designations[i]) for i in range(len(self.tiles))]

        # TODO where and how should this be initialized
        self.active_layer = 3

    def __contains__(self, key: T.Union[Entity, T.Tuple[int, int], T.Tuple[int, int, int]]) -> bool:
        if isinstance(key, Entity):
            return key in self._entities
        elif len(key) == 2:
            return (0, *key) in self
        else:
            return all(low <= p < hi for low, p, hi in zip((0, 0, 0), key, self.tiles.shape))

    timescale = event_property('_timescale', 'on_set_timescale')
    cursor = event_property('_cursor', 'on_set_cursor')
    paused = event_property('_paused', 'on_set_pause_state')

    @property
    def active_layer(self) -> int:
        """The index of the world grid z-layer currently in focus.

        Setting the value of this property will fire a
        `on_set_active_layer` event, and may trigger some
        potentially-expensive overlay-redrawing operations.
        """
        return self._active_layer

    @active_layer.setter
    def active_layer(self, value: int):
        self._active_layer = value
        self.dispatch_event('on_set_active_layer', value)
        if self.draw_atmosphere_overlay:
            self._update_atmosphere_colorscale()

    @property
    def draw_atmosphere_overlay(self) -> bool:
        """If ``True``, draw the atmosphere overlay.

        Setting the value of this property will fire a
        `on_set_atmosphere_overlay_display` event.
        """
        return self._draw_atmosphere_overlay

    @draw_atmosphere_overlay.setter
    def draw_atmosphere_overlay(self, value: bool):
        self._draw_atmosphere_overlay = value
        self.dispatch_event('on_set_atmosphere_overlay_display', value)
        if value:
            self._update_atmosphere_colorscale()

    @property
    def shape(self) -> T.Tuple[int, int, int]:
        return self.tiles.shape

    @with_default_location
    def get_tile(self, z: int, x: int, y: int) -> T.Optional[tile.TileType]:
        if (z, x, y) in self:
            return tile.TileType(self.tiles[(z, x, y)])

    @with_default_location
    def set_tile(self, z: int, x: int, y: int, tiletype: tile.TileType):
        if (z, x, y) in self:
            if self.tiles[z, x, y] != tiletype.value:
                self.tiles[z, x, y] = tiletype.value
                self._render_tree[z].refresh(x, y)
                if z == self.active_layer and (x, y) == self.cursor:
                    self.dispatch_event('on_set_cursor', self.cursor)

    @with_default_location
    def destroy_tile(self, z: int, x: int, y: int):
        """Destroy a tile and apply tile destruction logic."""
        ttype = self.get_tile(z, x, y)
        if ttype is not None:
            if ttype != tile.TileType.VOID:
                self.set_tile(z, x, y, tiletype=tile.TileType.VOID)
                for drop_factory in items.tile_drops(ttype).sample():
                    self.add_entity(drop_factory(z, x, y))

    @with_default_location
    def get_designation(self, z: int, x: int, y: int) -> T.Optional[Designation]:
        if (z, x, y) in self:
            return Designation(self.designations[z, x, y])

    @with_default_location
    def set_designation(self, z: int, x: int, y: int, designation: Designation):
        if (z, x, y) in self:
            if self.designations[z, x, y] != designation.value:
                old_designation = self.designations[z, x, y]
                self.designations[z, x, y] = designation.value
                self._render_tree[z].render_designation(Designation(old_designation))
                self._render_tree[z].render_designation(designation)
                if z == self.active_layer and (x, y) == self.cursor:
                    self.dispatch_event('on_set_cursor', self.cursor)

    def add_entity(self, entity: Entity):
        """Add an entity to this world."""
        entity.world = self
        self._entities.append(entity)

    def remove_entity(self, entity: Entity):
        """Remove an entity from this world.

        Note that the entity will not be removed until the next world
        step.

        This is equivalent to setting ``entity.active = False``,
        except that this method will raise if the given entity is not
        a member of this world.
        """
        if entity in self._entities:
            entity.active = False
        else:
            raise ValueError(f"Entity {entity} not in world {self}")

    def iter_entities(self) -> T.Iterable[Entity]:
        """Iterate over this world's entities."""
        yield from self._entities

    @property
    def entities(self) -> T.List[Entity]:
        """The entities tracked by this world."""
        return list(self.iter_entities())

    def entities_at_location(self, location: T.Tuple[int, int, int]) -> T.Iterable[Entity]:
        """Iterate over entities at a specific world grid location."""
        for e in self.iter_entities():
            if e.grid_location == location:
                yield e

    def entities_in_region(self,
                           start: T.Tuple[int, int, int],
                           end: T.Tuple[int, int, int]) -> T.Iterable[Entity]:
        """Iterate over entities within a rectangular region of the world grid, inclusive."""
        lower = [min(s, e) for s, e in zip(start, end)]
        upper = [max(s, e) for s, e in zip(start, end)]

        for entity in self.iter_entities():
            if all(a <= e <= b for a, e, b in zip(lower, entity.location, upper)):
                yield entity

    def entities_in_layer(self, z: int) -> T.Iterable[Entity]:
        for e in self.iter_entities():
            if int(e.z) == z:
                yield e

    def closest_entities(self,
                         point: T.Tuple[float, float, float],
                         predicate: T.Optional[T.Callable[[Entity], bool]] = None,
                         descending: bool = False) -> T.Iterable[Entity]:
        """Iterate over entities, sorted by distance from a point."""
        point = np.array(point)
        if predicate is not None:
            entities = [e for e in self.iter_entities() if predicate(e)]
        else:
            entities = self._entities

        if len(entities) > 0:
            locations = np.array([e.location for e in entities])

            order = np.argsort(np.sum(np.square(locations), axis=1))
            if not descending:
                order = order[::-1]
            for idx in order:
                yield entities[idx]

    def _update_atmosphere(self):
        """Run one step of atmosphere simulation.

        This is run once every `_ATMOSPHERE_UPDATE_STEPS` steps, by
        default 4.
        """
        logger.debug("Running atmosphere update.")
        if self._atmosphere_update_future is not None:
            self.atmosphere = self._atmosphere_update_future.result(timeout=None)
            if self.draw_atmosphere_overlay:
                self._update_atmosphere_colorscale()
            self.dispatch_event('on_atmosphere_update')

        impassable = ~np.isin(self.tiles, [t.value for t in tile.PASSABLE])
        self._atmosphere_update_future = eventloop.get().submit(
            extensions.simulate_heatmap,
            self.atmosphere, impassable, rate=_ATMOSPHERE_DISPERSION_RATE
        )

    def _update_atmosphere_colorscale(self):
        # XXX can be expensive...
        self._atmosphere_vlist.colors[:] = render.heatmap_colorscale(
            self.atmosphere[self.active_layer],
            scale_color=(0, 255, 255, 0),
            base_color=(255, 0, 0, 100)
        )

    def step(self):
        """Run one step of game logic.

        This is called by `World.update` after adjusting for the
        current timestep.
        """
        self._atmosphere_substeps = (self._atmosphere_substeps + 1) % _ATMOSPHERE_UPDATE_STEPS
        if self._atmosphere_substeps == 0:
            self._update_atmosphere()

        for e in self.entities:
            try:
                e.step()
            except GameLogicException as ex:
                logger.error(f"Logic exception during update for entity {e}:\n{ex}")
            if not e.active:
                self._entities.remove(e)
                e.dispatch_event('on_remove', e)

    def update(self, dt: float):
        if not self.paused:
            self._ms_since_last_step += dt * 1000.0
            if self._ms_since_last_step >= self.timescale.value:
                try:
                    self.step()
                except GameLogicException as ex:
                    logger.error(f"Logic exception during world update:\n{ex}")
                self._ms_since_last_step = 0

    def render(self):
        for i, layer in enumerate(self._render_tree):
            logger.info(f"rendering layer {i}")
            layer.render()

    def draw(self):
        render.draw_world_grid(self)
        render.draw_overlays(self)
        render.draw_cursor(self)

    def draw_bounds(self):
        render.draw_debug_overlay(self)

    def find_path(self,
                  start: T.Tuple[int, int, int],
                  end: T.Tuple[int, int, int],
                  entity: Entity,
                  fly: bool = False) -> T.Optional[Path]:
        if start not in self or end not in self:
            # Invalid path -- start or end is not in the world
            return None

        ez, ex, ey = end

        # A* pathfinding -- with a thank you to Nicholas Swift
        # see https://medium.com/@nicholas.w.swift/easy-a-star-pathfinding-7e6689c7f7b2
        # TODO vectorize

        @dataclass
        class Node:
            z: int
            x: int
            y: int
            parent: T.Optional[Node] = None

            @property
            def location(self) -> T.Tuple[int, int, int]:
                return self.z, self.x, self.y

            @property
            def f(self) -> int:
                return self.g + self.h

            @property
            def g(self) -> int:
                if self.parent is None:
                    return 0
                else:
                    return self.parent.g + 1

            @property
            def h(self) -> int:
                return (ez - self.z)**2 + (ex - self.x)**2 + (ey - self.y)**2

            def iter_path(self) -> T.Iterable[T.Tuple[int, int, int]]:
                if self.parent is not None:
                    yield from self.parent.iter_path()
                yield self.location

            def __eq__(self, other: Node) -> bool:
                return self.f == other.f

            def __lt__(self, other: Node) -> bool:
                return self.f < other.f

            def __gt__(self, other: Node) -> bool:
                return self.f > other.f

        open = queue.PriorityQueue()
        closed = []
        open.put(Node(*start))

        # D) Stop when the open list is empty (failure)
        while open.qsize() > 0:
            # A) Get lowest F-cost node on the open list
            current = open.get()

            if current.location == end:
                # D) Stop when you add the target to the closed list
                return Path(world=self, path=np.array(list(current.iter_path())).T)

            # B) Switch it to the closed list
            closed.append(current.location)

            offsets = [
                (0, 0, 1),
                (0, 1, 0),
                (0, 0, -1),
                (0, -1, 0)
            ]
            current_tile = self.get_tile(*current.location)
            if fly:
                offsets.append((1, 0, 0))
                if current_tile.open:
                    offsets.append((-1, 0, 0))
            else:
                if current_tile.ascendable:
                    offsets.append((1, 0, 0))
                if current_tile.descendable:
                    offsets.append((-1, 0, 0))

            # C) For each adjacent node...
            for idx in np.array(current.location) + np.array(offsets):
                idx = tuple(idx)
                # If it is not walkable or is on the closed list, ignore it
                if idx not in closed:
                    if entity.can_pass(idx):
                        for n in open.queue:
                            if n.location == idx:
                                # If it is on the open list, replace if path is better
                                if current.g + 1 < n.g:
                                    n.parent = current
                                break
                        else:
                            # If it is not on the open list, add it
                            open.put(Node(*idx, parent=current))
