"""Game world tiles"""
from enum import Enum

import pyglet

from ...resource import textures


class TileType(Enum):
    VOID = 0
    FLOOR = 1
    WALL = 2
    LADDER = 3
    STAIRS_NE = 4
    STAIRS_NW = 5
    STAIRS_SW = 6
    STAIRS_SE = 7

    @property
    def texture(self) -> pyglet.image.Texture:
        return textures.TILESHEET[self.value]

    @property
    def display_name(self) -> str:
        return _TILE_DISPLAY_NAMES.get(self, "unknown")

    @property
    def passable(self) -> bool:
        """Does this tile permit entities to move through it within a layer?"""
        return self in PASSABLE

    @property
    def open(self) -> bool:
        """Does this tile let things fall onto the layer below?"""
        return self in OPEN

    @property
    def ascendable(self) -> bool:
        """Does this tile permit walking access to/from the layer above?"""
        return self in ASCENDABLE

    @property
    def descendable(self) -> bool:
        """Does this tile permit walking access to/from the layer below?"""
        return self in DESCENDABLE



# TODO i18n
_TILE_DISPLAY_NAMES = {
    TileType.VOID: "open space",
    TileType.FLOOR: "floor",
    TileType.WALL: "wall",
    TileType.LADDER: "ladder",
    TileType.STAIRS_NE: "stairs",
    TileType.STAIRS_NW: "stairs",
    TileType.STAIRS_SW: "stairs",
    TileType.STAIRS_SE: "stairs",
}

PASSABLE = [
    TileType.VOID,
    TileType.FLOOR,
    TileType.LADDER,
    TileType.STAIRS_NE,
    TileType.STAIRS_NW,
    TileType.STAIRS_SW,
    TileType.STAIRS_SE,
]

OPEN = [
    TileType.VOID
]

ASCENDABLE = [
    TileType.LADDER,
    TileType.STAIRS_NE,
    TileType.STAIRS_NW,
    TileType.STAIRS_SW,
    TileType.STAIRS_SE,
]

DESCENDABLE = [
    TileType.VOID,
    TileType.LADDER,
]
