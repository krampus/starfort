"""Elements used to render the game world."""
from __future__ import annotations

import typing as T
import logging
from dataclasses import dataclass
from contextlib import contextmanager

import numpy as np
import pyglet
from pyglet import gl

from . import tile
from .. import TILE_H, grid_to_screen
from ...resource import textures
from ...ui import UITexture

if T.TYPE_CHECKING:
    from ..entities import Entity
    from . import World


logger = logging.getLogger(__name__)

_HIGHLIGHT_COLOR = (0.4, 1.0, 1.0, 0.5)
_WHITE = (1.0, 1.0, 1.0, 1.0)

_MAX_LAYER_TRAIL = 4

# A big, black, transparent quad to simulate "darkness" effects
_MAX = 99999
_ASTEP = 50
debug_big_shadow = pyglet.graphics.vertex_list(
    4,
    ('v2i', (-_MAX, -_MAX, _MAX, -_MAX, _MAX, _MAX, -_MAX, _MAX)),
    ('c4b', (0, 0, 0, _ASTEP, 0, 0, 0, _ASTEP, 0, 0, 0, _ASTEP, 0, 0, 0, _ASTEP))
)

tile_shape_verts = np.array(
    [
        [0, 0.25],
        [0.5, 0],
        [1, 0.25],
        [0.5, 0.5]
    ]) * textures.TILE_DIM - [textures.DEFAULT_ANCHOR_X, textures.DEFAULT_ANCHOR_Y]


def quad_grid(shape: T.Tuple[int, int]) -> np.ndarray:
    """Build a grid corresponding to the shapes of world tiles.

    This is used for overlays, but not for actual world grid
    rendering. World grid tiles are bitmap sprites, and must therefore
    be mapped to quads aligned to the screen axes.
    """
    indices = np.indices(shape)
    screen_coords = np.array(grid_to_screen(indices[0], indices[1]))
    verts = screen_coords + tile_shape_verts.reshape(4, 2, 1, 1)
    return verts.transpose(3, 2, 0, 1).reshape(-1)


def debug_grid(layer_shape: T.Tuple[int, int]) -> pyglet.graphics.vertexdomain.VertexList:
    """Build a vertex list for a debug grid overlay."""
    verts = quad_grid(layer_shape)
    n_verts = verts.size // 2
    return pyglet.graphics.vertex_list(
        n_verts,
        ('v2f', verts),
        ('c4B', (255, 255, 255, 200) * n_verts)
    )


def heatmap_colorscale(heatmap: np.ndarray,
                       scale_color: T.Tuple[int, int, int, int],
                       base_color: T.Tuple[int, int, int, int]) -> np.ndarray:
    """Build a vertex color list based on the values of a heatmap."""
    return (
        heatmap * np.array(scale_color * 4).reshape(4, 4, 1, 1)
        + np.array(base_color).reshape(4, 1, 1)
        ).astype(np.uint8).transpose(3, 2, 0, 1).reshape(-1)


class SliceGroup(pyglet.graphics.OrderedGroup):
    def __init__(self, order: int, texture: pyglet.image.Texture, **kwargs):
        super().__init__(order, **kwargs)
        self.texture = texture

    def set_state(self):
        gl.glEnable(self.texture.target)
        gl.glBindTexture(self.texture.target, self.texture.id)

    def unset_state(self):
        gl.glDisable(self.texture.target)


@dataclass
class Slice:
    order: int

    def __post_init__(self):
        self._tile_group = self._entity_group = None

    @property
    def tile_group(self) -> SliceGroup:
        if self._tile_group is None:
            self._tile_group = SliceGroup(self.order, texture=textures.TILESHEET)
        return self._tile_group

    @property
    def entity_group(self) -> SliceGroup:
        if self._entity_group is None:
            # Tile rendering orders are integers, so we offset our
            # render order by 0.5 to sandwich between the tile we're
            # standing on and the tile one slice in front of us (which
            # may occlude us)
            self._entity_group = SliceGroup(self.order + 0.5, texture=textures.ENTITYSHEET)
        return self._entity_group


class DesignationGroup(pyglet.graphics.Group):
    def __init__(self, texture: pyglet.image.Texture,
                 color: T.Tuple[float, float, float, float] = _WHITE,
                 **kwargs):
        super().__init__(**kwargs)
        self.texture = texture
        self.color = color

    def set_state(self):
        gl.glEnable(self.texture.target)
        gl.glBindTexture(self.texture.target, self.texture.id)
        gl.glColor4f(*self.color)

    def unset_state(self):
        gl.glColor4f(*_WHITE)
        gl.glDisable(self.texture.target)


def _build_designation_groups():
    from . import Designation
    return {
        Designation.FORBID: DesignationGroup(texture=UITexture.X.texture,
                                             color=(1., 0., 0., 0.8)),
        Designation.PRIORITY: DesignationGroup(texture=UITexture.IMPORTANT.texture,
                                               color=(0., 1., 0., 0.8)),
        Designation.DESTROY: DesignationGroup(texture=UITexture.LASER.texture,
                                              color=(1.0, 0.2, 0.1, 0.8))
    }


def layer_factory(layer_shape: T.Tuple[int, int]) -> type:
    from . import Designation

    height, width = layer_shape
    indices = np.indices(layer_shape)

    group_indices = [
        indices.diagonal(offset=i, axis1=1, axis2=2)
        for i in range(layer_shape[1] - 1, -layer_shape[0], -1)
    ]

    slice_map = np.empty(layer_shape, dtype=object)
    for i, idx in enumerate(group_indices):
        slice = Slice(i)
        xi, yi = idx
        slice_map[xi, yi] = slice

    screen_coords = np.array(grid_to_screen(indices[0], indices[1]))
    offsets = (np.array([
        [0, 0],
        [1, 0],
        [1, 1],
        [0, 1]
    ]) * textures.TILE_DIM - [textures.TILESHEET[0].anchor_x, textures.TILESHEET[0].anchor_y]
    ).reshape(4, 2, 1, 1)
    verts = screen_coords + offsets

    designation_verts = verts + np.array([
        [1, 1],
        [-1, 1],
        [-1, -1],
        [1, -1]
    ]).reshape(4, 2, 1, 1) * 0.25 * textures.TILE_DIM
    designation_groups = _build_designation_groups()

    class RenderLayer:
        """World grid rendering structure, representing one z-level or "layer".

        Sprites are batched per layer, allowing us to selectively render
        layers. Within a layer, sprites are grouped based on screen
        order, allowing for apparent depth in the isometric view.
        """
        def __init__(self, layer: np.ndarray, designations: np.ndarray):
            self.layer = layer
            self.designations = designations

            self.batch = pyglet.graphics.Batch()
            self.designation_batch = pyglet.graphics.Batch()

            self.vlists = np.empty_like(layer, dtype=object)
            self.designation_vlists = {}

        def render(self):
            """Build the vertex list for this layer.

            This can be an extremely expensive operation. Typically, it
            only needs to be called once, after the underlying layer
            is populated.
            """
            # render tiles
            for (x, y), t in np.ndenumerate(self.layer):
                t = tile.TileType(t)
                if self.vlists[x, y] is not None:
                    self.vlists[x, y].delete()
                self.vlists[x, y] = self.batch.add(
                    4, gl.GL_QUADS, slice_map[x, y].tile_group,
                    ('v2f', verts[:, :, x, y].reshape(-1)),
                    ('t3f', t.texture.tex_coords)
                )

            # render designations
            for dtype in Designation:
                self.render_designation(dtype)

        def render_designation(self, dtype: Designation):
            if dtype in self.designation_vlists:
                self.designation_vlists[dtype].delete()

            # don't try to render designations we don't want to render
            if dtype in designation_groups:
                group = designation_groups[dtype]

                x, y = np.where(self.designations == dtype.value)
                n_quads = len(x)
                self.designation_vlists[dtype] = self.designation_batch.add(
                    n_quads * 4, gl.GL_QUADS, group,
                    ('v2f', designation_verts[:, :, x, y].transpose(2, 0, 1).reshape(-1)),
                    ('t3f', group.texture.tex_coords * n_quads)
                )
            self.designation_batch.invalidate()

        def refresh(self, x: int, y: int):
            self.vlists[x, y].tex_coords = tile.TileType(self.layer[x, y]).texture.tex_coords

        def draw(self, entities: T.Iterable[Entity]):
            for e in entities:
                if e.visible:
                    sx, sy = grid_to_screen(e.x, e.y)
                    e.sprite.update(x=sx, y=sy)
                    # XXX optimize this
                    e.sprite.group = slice_map[e.x, e.y].entity_group
                    e.sprite.batch = self.batch
            self.batch.draw()

            self.designation_batch.draw()

    return RenderLayer


@contextmanager
def gl_context():
    gl.glPushMatrix()

    gl.glColor4f(1., 1., 1., 1.)
    gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)

    yield

    gl.glPopMatrix()


def draw_world_grid(world: World):
    """Draw a world's world grid.

    The active layer is drawn front and center. To give players more
    spatial context, we also draw a "layer trail" of the next handful
    of layers below the active layer, shaded out to indicate that
    they're not the player's focus.
    """
    with gl_context():
        trail_start = max(world.active_layer - _MAX_LAYER_TRAIL, 0)
        trail_range = range(trail_start, world.active_layer + 1)

        def draw_trail(trail):
            *head, tail = trail
            if len(head) > 0:
                gl.glPushMatrix()
                gl.glTranslatef(0.0, -TILE_H, 0.0)
                draw_trail(head)
                debug_big_shadow.draw(gl.GL_QUADS)
                gl.glPopMatrix()
            # XXX this can be optimized
            ents = filter(lambda e: e.z == tail, world._entities)
            world._render_tree[tail].draw(ents)
        draw_trail(trail_range)


def draw_overlays(world: World):
    """Draw overlays on top of the game world.

    These toggleable overlays display useful information (like
    atmosphere and designations) which players will need only under
    certain circumstances.
    """
    if world.draw_atmosphere_overlay:
        world._atmosphere_vlist.draw(gl.GL_QUADS)
        gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
        world._atmosphere_vlist.draw(gl.GL_QUADS)
        gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)


def draw_debug_overlay(world: World):
    if not hasattr(world, '_debug_grid'):
        world._debug_grid = debug_grid(world.shape[1:])

    gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
    world._debug_grid.draw(gl.GL_QUADS)
    gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)


def draw_cursor(world: World):
    """Draw the world cursor.

    The cursor should be highlighted, and should be semi-visible
    through occluding foreground objects.
    """
    if world.cursor is not None and world.cursor in world:
        gx, gy = world.cursor
        sx, sy = grid_to_screen(gx, gy)
        ttype = tile.TileType(world.tiles[world.active_layer, gx, gy])
        gl.glColor4f(*_HIGHLIGHT_COLOR)
        if ttype == tile.TileType.VOID:
            pyglet.graphics.draw(
                4, gl.GL_LINE_LOOP,
                ('v2f', (tile_shape_verts + (sx, sy)).reshape(-1))
            )
        else:
            ttype.texture.blit(sx, sy)

        gl.glColor4f(*_WHITE)
