"""Item entities."""
from __future__ import annotations

from dataclasses import dataclass
import typing as T

import pyglet

from . import Entity
from .. import NamedMixin, model
from ..world.tile import TileType

if T.TYPE_CHECKING:
    from .actors import Actor


@dataclass
class Item(Entity):
    """Base class for items.

    Items are entities that can be picked up and stored in containers.
    """
    container: T.Optional[Container] = None

    # Derived types should override these as needed.
    mass = 0.0
    size = 0.0

    def step(self):
        # Items just sit there!
        pass


class Stone(Item):
    """A rock of no great import."""
    mass = 1.0
    size = 10.0

    texture_index = 4


class Ore(Item):
    """A rock of great import."""
    mass = 3.0
    size = 10.0

    texture_index = 4

    def __post_init__(self):
        super().__post_init__()
        self.sprite.color = (255, 50, 25)


class Steel(Item):
    """A reasonably common alloy of iron and carbon."""
    mass = 1.0
    size = 1.0

    texture_index = 5


class Container(pyglet.event.EventDispatcher, NamedMixin):
    """A container that can hold items."""
    # Derived types should override these as needed.
    mass_limit: T.Optional[float] = None
    size_limit: T.Optional[float] = None
    quantity_limit: T.Optional[int] = None

    def __init__(self, contents: T.Iterable[Item] = tuple()):
        super().__init__()
        self.register_event_type('on_update_contents')
        self.contents: T.List[Item] = []
        for i in contents:
            self.add(i)

    def __contains__(self, key: Item) -> bool:
        return key in self.contents

    def __iter__(self) -> T.Iterable[Item]:
        yield from self.contents

    def __len__(self) -> int:
        return len(self.contents)

    @property
    def contained_mass(self) -> float:
        return sum(i.mass for i in self.contents)

    @property
    def contained_size(self) -> float:
        return sum(i.size for i in self.contents)

    def can_hold(self, item: Item) -> bool:
        if self.mass_limit is not None and item.mass + self.contained_mass > self.mass_limit:
            return False

        if self.size_limit is not None and item.size + self.contained_size > self.size_limit:
            return False

        if self.quantity_limit is not None and len(self) + 1 > self.quantity_limit:
            return False

        return True

    def can_be_accessed(self, actor: Actor) -> bool:
        """Check if this container can be accessed by the given actor."""
        raise NotImplementedError("Implemented by derived type.")

    def add(self, item: Item):
        """Add an item to this container.

        Note that this does not check if the container can hold the
        given item! Make sure to call ``can_hold`` first if you want
        to apply game logic.
        """
        if item.container is not None:
            item.container.remove(item)

        item.active = False
        item.container = self
        self.contents.append(item)
        self.dispatch_event('on_update_contents', self.contents)

    def remove(self, item: Item):
        """Remove an item from this container."""
        item.active = True
        item.container = None
        self.contents.remove(item)
        self.dispatch_event('on_update_contents', self.contents)


class Chest(Entity, Container):
    """Base for containers which are themselves entities."""
    def __init__(self, *args, contents: T.Iterable[Item] = tuple(), **kwargs):
        Entity.__init__(self, *args, **kwargs)
        Container.__init__(self, contents=contents)

    def can_be_accessed(self, actor: Actor) -> bool:
        return actor.can_reach(self)

    def step(self):
        pass


class CargoPod(Chest):
    """A basic chest-like container entity."""
    size_limit = 100.0
    texture_index = 6


class ItemDistribution:
    """A probabilistic distribution of items.

    This is used for modelling processes that generate items with some
    sort of known distribution, like rubble dropping from a destroyed rock.
    """
    def __init__(self, *distribution: T.Tuple[T.Callable[[], Item], model.AbstractRV]):
        self.distribution = distribution

    def _iter_sample(self, count: int = 1) -> T.Iterable[T.Callable[[], Item]]:
        for factory, rv in self.distribution:
            outcomes = rv.sample(count)
            for outcome in outcomes:
                for _ in range(int(outcome)):
                    yield factory

    def sample(self, count: int = 1) -> T.List[T.Callable[[], Item]]:
        return list(self._iter_sample(count))


# Loot table for tile drops
# XXX where and how should this be defined?
_TILE_DROPS = {
    TileType.WALL: ItemDistribution(
        (Steel, model.BinomialRV(n=6, p=0.25))
    ),
    TileType.STAIRS_NE: ItemDistribution(
        (Steel, model.BinomialRV(n=3, p=0.25))
    ),
    TileType.STAIRS_NW: ItemDistribution(
        (Steel, model.BinomialRV(n=3, p=0.25))
    ),
    TileType.STAIRS_SW: ItemDistribution(
        (Steel, model.BinomialRV(n=3, p=0.25))
    ),
    TileType.STAIRS_SE: ItemDistribution(
        (Steel, model.BinomialRV(n=3, p=0.25))
    ),
}


def tile_drops(ttype: TileType) -> T.Optional[ItemDistribution]:
    return _TILE_DROPS.get(ttype, None)
