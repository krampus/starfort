"""Actors -- entities with behaviors"""
from __future__ import annotations

import typing as T
from dataclasses import dataclass

from . import Entity, behaviors, items
from .. import event_property

if T.TYPE_CHECKING:
    from ..world import Path


@dataclass
class Actor(Entity):
    """An entity that has behaviors and capabilities."""

    behavior = event_property('_behavior', 'on_set_behavior')

    def __post_init__(self):
        super().__post_init__()
        self.register_event_type('on_set_behavior')
        self._behavior = self._default_behavior()
        self.state = behaviors.BehaviorState.IDLE

    def _default_behavior(self) -> behaviors.Behavior:
        return behaviors.StandStill(self)

    def can_pass(self, index: T.Tuple[int, int, int]) -> bool:
        """Can this actor pass through the given world grid location?"""
        tile = self.world.get_tile(*index)
        if tile is not None and tile.passable:
            if tile.open:
                z, x, y = index
                below = self.world.get_tile(z - 1, x, y)
                return below is not None and (below.ascendable or not below.passable)
            else:
                return True
        return False

    def can_reach(self, other: Entity) -> bool:
        """Can this actor reach another entity?"""
        if self.world == other.world and self.z == other.z:
            return -1 <= self.x - other.x <= 1 and -1 <= self.y - other.y <= 1
        else:
            return False

    def find_path(self, target: T.Tuple[int, int, int]) -> T.Optional[Path]:
        return self.world.find_path(self.location, target, self)

    def step(self):
        # TODO I thought this would be good but now I'm not so sure...
        try:
            # TODO what to do with this?
            self.state = next(self.behavior)
        except StopIteration:
            self.behavior = self._default_behavior()


class Inventory(items.Container):
    """A container for things held by a creature."""
    def __init__(self, owner: Creature, *args, **kwargs):
        self.owner = owner
        super().__init__(*args, **kwargs)

    @property
    def display_name(self) -> str:
        # TODO i18n
        return f"{self.owner.display_name}'s inventory"

    def can_be_accessed(self, actor: Actor) -> bool:
        return actor is self.owner

    @property
    def mass_limit(self) -> T.Optional[float]:
        return self.owner.inventory_mass_limit

    @property
    def size_limit(self) -> T.Optional[float]:
        return self.owner.inventory_size_limit

    @property
    def quantity_limit(self) -> T.Optional[int]:
        return self.owner.inventory_quantity_limit


class Creature(Actor):
    """An actor representing a living thing.

    Creatures are alive and can die. They can also carry things in
    their inventory.
    """
    # Derived types should override these as needed.
    inventory_mass_limit: T.Optional[float] = None
    inventory_size_limit: T.Optional[float] = None
    inventory_quantity_limit: T.Optional[int] = None

    def __post_init__(self):
        super().__post_init__()
        self.register_event_type('on_death')
        self.inventory = Inventory(self)

    @property
    def alive(self) -> bool:
        raise NotImplementedError("Implemented by derived type.")

    def can_reach(self, other: Entity) -> bool:
        if other in self.inventory:
            return True
        else:
            return super().can_reach(other)

    def make_remains(self) -> T.Optional[items.Item]:
        """Construct an item representing this creature after death."""
        return None

    def drop(self, item: items.Item):
        """Drop an item in our inventory onto the world."""
        self.inventory.remove(item)
        item.location = self.location
        self.world.add_entity(item)

    def die(self):
        """Drop dead and leave a beautiful corpse (if any)."""
        remains = self.make_remains()

        # Leave what we can on our body
        if remains is not None:
            if isinstance(remains, items.Chest):
                for i in list(self.inventory):
                    if remains.can_hold(i):
                        self.inventory.remove(i)
                        remains.add(i)
            self.world.add_entity(remains)

        # Drop everything we didn't leave on our body
        for i in list(self.inventory):
            self.drop(i)

        self.active = False

    def step(self):
        super().step()
        if not self.alive:
            self.die()


@dataclass
class SimpleCreature(Creature):
    """A creature with a simple floating-point value for a life bar."""
    max_life: float = 100.0

    def __post_init__(self):
        super().__post_init__()
        self.life: float = self.max_life

    @property
    def alive(self) -> bool:
        return self.life > 0


@dataclass
class Mite(SimpleCreature):
    """A little spacemite! Used for entity debugging."""

    max_life = 10.0
    inventory_mass_limit = 1.0
    texture_index = 2

    def _default_behavior(self) -> behaviors.Behavior:
        return behaviors.Wander(self, p_move=0.05)


class Pawn(Creature):
    """Base class for an actor that follows directions from the player."""
    texture_index = 3
