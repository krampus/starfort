"""Actor behaviors"""
from __future__ import annotations

import typing as T
from dataclasses import dataclass
from enum import Enum, auto
import random
import logging

import numpy as np

if T.TYPE_CHECKING:
    from .actors import Actor, Creature
    from .items import Item, Container

logger = logging.getLogger(__name__)


_NEIGHBOR_OFFSETS = np.array([
    (-1, 0, 0),
    (1, 0, 0),
    (0, -1, 0),
    (0, 1, 0),
    (0, 0, -1),
    (0, 0, 1)
])


def _location_property(docstring=None):
    """Shortcut for a property that exposes a class's z, x, and y
    attributes as a point location."""

    def getter(self) -> T.Tuple[int, int, int]:
        return (self.z, self.x, self.y)

    return property(fget=getter, doc=docstring)


class BehaviorState(Enum):
    IDLE = auto()
    BUSY = auto()
    THINKING = auto()
    FAILED = auto()


@dataclass
class Behavior:
    """Base class for actor behaviors.

    Behaviors are things that actors can do.
    """
    actor: Actor
    display_noun = None
    display_imperative = None

    def __post_init__(self):
        self._state = self.iter_steps()

    def __iter__(self):
        return iter(self._state)

    def __next__(self):
        return next(self._state)

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        """Generate the actions of this behavior."""
        raise NotImplementedError("Implemented by derived type")


class BehaviorSequence(Behavior):
    """A complex behavior composed of other behaviors in sequence.

    Any behavior ending in failure will cause the entire sequence to fail.
    """
    def __init__(self, actor: Actor, *behaviors):
        self.sequence = behaviors
        super().__init__(actor)

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return ", ".join(b.display_noun for b in self.sequence)

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return ", ".join(b.display_imperative for b in self.sequence)

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        while len(self.sequence) > 0:
            head, *self.sequence = self.sequence
            for state in head:
                yield state
                if state == BehaviorState.FAILED:
                    return


class StandStill(Behavior):
    """Do nothing."""
    # TODO i18n
    display_noun = "standing still"
    display_imperative = "stand still"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        while True:
            yield BehaviorState.IDLE


@dataclass
class Move(Behavior):
    """Change location.

    Note that this behavior simply changes the location of the
    actor. Nothing between the actor's current location and the target
    location will be checked -- for moving an actor along a more
    complex path, see ``MoveToTarget``.
    """
    z: int
    x: int
    y: int

    target = _location_property()

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return f"moving to {(self.z, self.x, self.y)}"

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return f"move to {(self.z, self.x, self.y)}"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        # TODO integrate over several steps based on actor speed
        if self.actor.can_pass(self.target):
            self.actor.location = self.target
            yield BehaviorState.BUSY
        else:
            yield BehaviorState.FAILED


@dataclass
class Wander(Behavior):
    """Wandering about aimlessly."""
    p_move: float = 0.1
    same_level: bool = False

    # TODO i18n
    display_noun = "wandering"
    display_imperative = "wander"

    def _iter_neighbors(self) -> T.Iterable[T.Tuple[int, int, int]]:
        neighbors = _NEIGHBOR_OFFSETS + self.actor.location
        yield from neighbors[2:]

        if not self.same_level:
            actor_t = self.actor.relative_tile()
            # down
            if actor_t.descendable:
                yield neighbors[0]
            # up
            if actor_t.ascendable:
                yield neighbors[1]

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        while True:
            if random.random() <= self.p_move:
                options = list(self._iter_neighbors())
                random.shuffle(options)
                for z, x, y in options:
                    head, *tail = Move(self.actor, z, x, y)
                    if head is not None and head != BehaviorState.FAILED:
                        yield head
                        yield from tail
                        break
                else:
                    yield BehaviorState.IDLE
            else:
                yield BehaviorState.IDLE


@dataclass
class MoveToTarget(Behavior):
    """Advance towards a target."""
    z: int
    x: int
    y: int

    target = _location_property()

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return f"moving to location {(self.z, self.x, self.y)}"

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return f"move to location {(self.z, self.x, self.y)}"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        path = None

        def find_path() -> T.Iterable[BehaviorState]:
            # TODO distant paths should take several steps of "thinking"
            nonlocal path
            path = self.actor.find_path(self.target)
            yield BehaviorState.THINKING

        yield from find_path()

        while True:
            if path is None:
                logger.debug(f"Actor {self.actor} has no path to target {self.target}.")
                yield BehaviorState.FAILED
                return

            if len(path) > 0:
                z, x, y = path.pop()
                head, *tail = Move(self.actor, z, x, y)
                if head is not None and head != BehaviorState.FAILED:
                    yield head
                    yield from tail
                else:
                    # Movement interrupted, recompute path
                    logger.debug("Recomputing path...")
                    yield from find_path()
            else:
                # done moving
                logger.debug("Movement complete")
                yield BehaviorState.IDLE
                break


@dataclass
class AddToContainer(Behavior):
    """Take an item from the world and add it to a container."""
    item: Item
    container: Container

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return f"putting {self.item.display_name} in {self.container.display_name}"

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return f"put {self.item.display_name} in {self.container.display_name}"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        if self.actor.can_reach(self.item) \
           and self.container.can_be_accessed(self.actor) \
           and self.container.can_hold(self.item):
            self.container.add(self.item)
            yield BehaviorState.BUSY
        else:
            yield BehaviorState.FAILED


@dataclass
class RemoveFromContainer(Behavior):
    """Take an item from a container and place it in the world."""
    item: Item
    container: Container

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return f"removing {self.item.display_name} from {self.container.display_name}"

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return f"remove {self.item.display_name} from {self.container.display_name}"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        if self.item in self.container \
           and self.container.can_be_accessed(self.actor):
            self.container.remove(self.item)
            self.item.world = self.actor.world
            # XXX is this needed? Is it correct?
            self.item.location = self.actor.location
            yield BehaviorState.BUSY
        else:
            yield BehaviorState.FAILED


@dataclass
class TransferItem(Behavior):
    """Move an item from one container to another."""
    item: Item
    source: Container
    destination: Container

    @property
    def display_noun(self) -> str:
        # TODO i18n
        return f"moving {self.item.display_name} from {self.source.display_name} to {self.destination.display_name}"

    @property
    def display_imperative(self) -> str:
        # TODO i18n
        return f"move {self.item.display_name} from {self.source.display_name} to {self.destination.display_name}"

    def iter_steps(self) -> T.Iterable[BehaviorState]:
        if self.item in self.source \
           and self.source.can_be_accessed(self.actor) \
           and self.destination.can_be_accessed(self.actor) \
           and self.destination.can_hold(self.item):
            self.source.remove(self.item)
            self.destination.add(self.item)
            yield BehaviorState.BUSY
        else:
            yield BehaviorState.FAILED
