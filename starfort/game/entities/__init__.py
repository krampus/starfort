"""Game entities."""
from __future__ import annotations

from dataclasses import dataclass
import typing as T
import logging

import pyglet

from ..world.tile import TileType
from .. import NamedMixin
from ...resource import textures
from ...exceptions import GameLogicException

if T.TYPE_CHECKING:
    from ..world import World

logger = logging.getLogger(__name__)


@dataclass
class Entity(pyglet.event.EventDispatcher, NamedMixin):
    """Base class for game entities.

    A game entity is anything with a position on the game grid that
    isn't a tile.
    """
    # world grid coordinates
    _z: float
    _x: float
    _y: float

    # inactive entities are removed from the world at the end of the update
    active: bool = True

    # parent world
    _world: T.Optional[World] = None

    # spritesheet index in spacefort.resource.textures.ENTITYSHEET
    texture_index = 1

    def __post_init__(self):
        for event in ['on_move', 'on_remove']:
            self.register_event_type(event)
        self.sprite = pyglet.sprite.Sprite(self.texture)

    @property
    def z(self) -> float:
        return self._z

    @z.setter
    def z(self, value: float):
        self._z = value
        self.dispatch_event('on_move', self.z, self.x, self.y)

    @property
    def x(self) -> float:
        return self._x

    @x.setter
    def x(self, value: float):
        self._x = value
        self.dispatch_event('on_move', self.z, self.x, self.y)

    @property
    def y(self) -> float:
        return self._y

    @y.setter
    def y(self, value: float):
        self._y = value
        self.dispatch_event('on_move', self.z, self.x, self.y)

    @property
    def location(self) -> T.Tuple[float, float, float]:
        return self.z, self.x, self.y

    @location.setter
    def location(self, value: T.Tuple[float, float, float]):
        # TODO bounds checking?
        self._z, self._x, self._y = value
        self.dispatch_event('on_move', self.z, self.x, self.y)

    @property
    def grid_location(self) -> T.Tuple[int, int, int]:
        return int(self.z), int(self.x), int(self.y)

    @property
    def texture(self) -> pyglet.graphics.Texture:
        return textures.ENTITYSHEET[self.texture_index]

    @property
    def visible(self) -> bool:
        return self.sprite.visible

    @visible.setter
    def visible(self, value: bool):
        self.sprite.visible = value

    @property
    def tile(self) -> T.Optional[TileType]:
        return self.relative_tile()

    @property
    def world(self) -> World:
        if self._world is not None:
            return self._world
        else:
            raise GameLogicException("Entity {self} is not associated with any World.")

    @world.setter
    def world(self, value: World):
        self._world = value

    def relative_tile(self, dz: int = 0, dx: int = 0, dy: int = 0) -> T.Optional[TileType]:
        """Get a tile relative to this entity's location.

        With no parameters, this returns the tile occupied by the
        entity.
        """
        return self.world.get_tile(self.z + dz, self.x + dx, self.y + dy)

    def move(self, dz: int = 0, dx: int = 0, dy: int = 0):
        """Move the entity relative to its current position."""
        # TODO bounds-check?
        self.location = (self.z + dz, self.x + dx, self.y + dy)

    def step(self):
        raise NotImplementedError("Implemented by derived type")

    def on_remove(self, e: Entity):
        self.world = None
        self.sprite.delete()


from . import behaviors, actors  # noqa E402
__all__ = [
    'behaviors',
    'actors',
]
