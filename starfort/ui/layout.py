"""Components that organize other components."""

import typing as T
import logging

import numpy as np

from . import WindowComponent, Axis, Alignment, Unbounded, cached, cachedattr

logger = logging.getLogger(__name__)


class WindowLayout(WindowComponent):
    """A windowing component that organizes other components."""
    _debug_color = (0, 255, 0, 255)

    def __init__(self, *children: T.Iterable[T.Union[WindowComponent, dict]], **kwargs):
        self._children = []
        for child in children:
            if isinstance(child, dict):
                append_kwargs = child
                child = append_kwargs.pop("child")
            else:
                append_kwargs = {}

            self.append(child, **append_kwargs)
        super().__init__(**kwargs)

    def __len__(self) -> int:
        return len(self._children)

    def __contains__(self, key: WindowComponent) -> bool:
        return key in self._children

    def __getitem__(self, key: int) -> WindowComponent:
        return self._children[key]

    def __setitem__(self, key: int, value: WindowComponent):
        value._depends.add(self)
        self._children[key] = value
        self.invalidate()

    def __delitem__(self, key: int):
        del self._children[key]
        self.invalidate()

    def append(self, child: WindowComponent):
        child._depends.add(self)
        self._children.append(child)
        self.invalidate()

    def remove(self, child: WindowComponent):
        self._children.remove(child)
        self.invalidate()

    def pop(self, index: int) -> WindowComponent:
        ret = self._children.pop(index)
        self.invalidate()
        return ret

    def iter_children(self) -> T.Iterable[WindowComponent]:
        yield from self._children

    @property
    def children(self) -> T.List[WindowComponent]:
        return list(self.iter_children())

    def _reposition(self, x: int = 0, y: int = 0):
        for child in self.iter_children():
            child.reposition(x=x, y=y)

    def draw(self):
        for child in self.iter_children():
            child.draw()

    def draw_debug(self):
        super().draw_debug()
        for child in self.iter_children():
            child.draw_debug()


class Layout1D(WindowLayout):
    def __init__(self, *args, align: Alignment = Alignment.START, **kwargs):
        self._align_default = align
        super().__init__(*args, **kwargs)

    def append(self, child: WindowComponent, *args, align: T.Optional[Alignment] = None, **kwargs):
        child._align = align or self._align_default
        super().append(child, *args, **kwargs)


class Layout2D(WindowLayout):
    def __init__(self, *args, h_align: Alignment = Alignment.START, v_align: Alignment = Alignment.START, **kwargs):
        self._h_align_default = h_align
        self._v_align_default = v_align
        super().__init__(*args, **kwargs)

    def append(self, child: WindowComponent, *args,
               h_align: T.Optional[Alignment] = None,
               v_align: T.Optional[Alignment] = None,
               **kwargs):
        child._h_align = h_align or self._h_align_default
        child._v_align = v_align or self._v_align_default
        super().append(child, *args, **kwargs)


class Stack(Layout2D):
    """Place components one on top of the other."""
    @property
    @cached
    def width(self) -> int:
        return max((child.width for child in self.children), default=0)

    @property
    @cached
    def height(self) -> int:
        return max((child.height for child in self.children), default=0)

    def _reposition(self, x: int = 0, y: int = 0):
        for child in self.children:
            child.reposition(
                x=x + child._h_align.offset(self.width, child.width),
                y=y + child._v_align.offset(self.height, child.height)
            )


class Absolute(Unbounded, WindowLayout):
    """Place components at absolute window positions."""

    def append(self, child: WindowComponent, x: int, y: int):
        child._x, child._y = x, y
        super().append(child)

    def _reposition(self, **kwargs):
        for child in self.children:
            child.reposition(x=getattr(child, '_x', 0), y=getattr(child, '_y', 0))


class Align(Layout1D):
    """Place components in a line."""
    def __init__(self, *args, axis: Axis = Axis.HORIZONTAL, **kwargs):
        self._axis = axis
        super().__init__(*args, **kwargs)

    axis = cachedattr('_axis')

    @property
    @cached
    def width(self) -> int:
        if self.axis == Axis.HORIZONTAL:
            return sum(child.width for child in self.children)
        else:
            return max((child.width for child in self.children), default=0)

    @property
    @cached
    def height(self) -> int:
        if self.axis == Axis.HORIZONTAL:
            return max((child.height for child in self.children), default=0)
        else:
            return sum(child.height for child in self.children)

    def _space_for(self, child: WindowComponent):
        if self.axis == Axis.HORIZONTAL:
            return child.width
        else:
            return child.height

    def _reposition(self, x: int = 0, y: int = 0):
        for child in self.children:
            if self.axis == Axis.HORIZONTAL:
                offset = child._align.offset(self.height, child.height)
                child.reposition(x=x, y=y + offset)
                x += self._space_for(child)
            else:
                offset = child._align.offset(self.width, child.width)
                child.reposition(x=x + offset, y=y)
                y += self._space_for(child)


class FixedAlign(Align):
    """Place components in a line, with constant space between them."""
    def __init__(self, *args, space: int, **kwargs):
        self._space = space
        super().__init__(*args, **kwargs)

    space = cachedattr('_space')

    @property
    @cached
    def width(self) -> int:
        if self.axis == Axis.HORIZONTAL:
            return max((i * self.space + child.width for i, child in enumerate(self.children)), default=0)
        else:
            return super().width

    @property
    @cached
    def height(self) -> int:
        if self.axis == Axis.HORIZONTAL:
            return super().height
        else:
            return max((i * self.space + child.height for i, child in enumerate(self.children)), default=0)

    def _space_for(self, child: WindowComponent):
        return self.space


class Grid(Layout2D):
    """Place components in a grid.

    Parameters
    ----------
    line_limit : int
        Number of children drawn per row or column, depending on `axis`.
    axis : Axis, optional
        Orientation of the grid. If this is ``Axis.HORIZONTAL``,
        children will fill up the rows of the grid in order, wrapping
        each row at `line_limit`. Conversely, if this is
        ``Axis.VERTICAL``, children will fill up the columns
        first. Defaults to ``Axis.HORIZONTAL``.
    """
    def __init__(self, *args, line_limit: int, axis: Axis = Axis.HORIZONTAL, **kwargs):
        self._line_limit = line_limit
        self._axis = axis
        super().__init__(**kwargs)

    line_limit = cachedattr('_line_limit')
    axis = cachedattr('_axis')

    def _child_grid(self) -> np.ndarray:
        linear = np.array(list(self.children) + ([None] * (-len(self) % self.line_limit)))
        if self.axis == Axis.HORIZONTAL:
            return linear.reshape(-1, self.line_limit)
        else:
            return linear.reshape(self.line_limit, -1).T

    @property
    @cached
    def width(self) -> int:
        grid = self._child_grid()
        return sum(
            max((child.width for child in col if child is not None), default=0)
            for col in grid.T
        )

    @property
    @cached
    def height(self) -> int:
        grid = self._child_grid()
        return sum(
            max((child.height for child in row if child is not None), default=0)
            for row in grid
        )

    def _reposition(self, x: int = 0, y: int = 0):
        grid = self._child_grid()
        dims = np.empty((2,) + grid.shape)
        for (i, j), child in np.ndenumerate(grid):
            dims[:, i, j] = (child.width, child.height) if child is not None else (np.NaN, np.NaN)

        column_widths = np.nanmax(dims[0], axis=0)
        row_heights = np.nanmax(dims[1], axis=1)
        space = np.array(np.meshgrid(column_widths, row_heights))

        reposition_coordinates = np.array(np.meshgrid(
            np.add.accumulate([0] + list(column_widths[:-1])) + x,
            np.add.accumulate([0] + list(row_heights[:-1])) + y
        ))
        for (i, j), child in np.ndenumerate(grid):
            if child is not None:
                cell_x, cell_y = reposition_coordinates[:, i, j]
                space_x, space_y = space[:, i, j]
                child.reposition(
                    x=cell_x + child._h_align.offset(space_x, child.width),
                    y=cell_y + child._v_align.offset(space_y, child.height)
                )


__all__ = [
    'Stack',
    'Absolute',
    'Align',
    'FixedAlign',
    'Grid',
]
