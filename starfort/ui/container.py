"""Components that contain other components."""

import typing as T
import logging

from . import WindowComponent, cached, cachedattr

logger = logging.getLogger(__name__)


class Container(WindowComponent):
    """A windowing component that contains a child component."""
    _debug_color = (255, 255, 255, 255)

    def __init__(self, child: T.Optional[WindowComponent] = None, **kwargs):
        self.child = child
        super().__init__(**kwargs)

    @property
    def child(self) -> WindowComponent:
        return self._child

    @child.setter
    def child(self, value: T.Optional[WindowComponent]):
        if value is not None:
            value._depends.add(self)

        self._child = value
        self.invalidate()

    @property
    @cached
    def width(self) -> int:
        return self.child.width if self.child else 0

    @property
    @cached
    def height(self) -> int:
        return self.child.height if self.child else 0

    def _reposition(self, x: int = 0, y: int = 0):
        if self.child:
            self.child.reposition(x=x, y=y)

    def draw(self):
        if self.child:
            self.child.draw()

    def draw_debug(self):
        super().draw_debug()
        if self.child:
            self.child.draw_debug()


class Fixed(Container):
    """Constrain a child component into a fixed space."""
    def __init__(self, width: int, height: int, **kwargs):
        self._width = width
        self._height = height
        super().__init__(**kwargs)

    width = cachedattr('_width')
    height = cachedattr('_height')

    def _reposition(self, x: int = 0, y: int = 0):
        # TODO
        super()._reposition(x=x, y=y)


class Scroll(Fixed):
    """Constrain a child component into a limited space with scroll bars."""
    pass  # TODO


class Margin(Container):
    """Add a buffer of empty space around a child component."""
    def __init__(self,
                 margin: int = None,
                 vertical: int = None, horizontal: int = None,
                 left: int = None, right: int = None, top: int = None, bottom: int = None,
                 **kwargs):
        if margin is not None:
            self._left = self._right = self._top = self._bottom = margin
        else:
            if vertical is not None or horizontal is not None:
                self._top = self._bottom = vertical or (0, 0)
                self._left = self._right = horizontal or (0, 0)
            else:
                self._left = left or 0
                self._right = right or 0
                self._top = top or 0
                self._bottom = bottom or 0
        super().__init__(**kwargs)

    left = cachedattr('_left')
    right = cachedattr('_right')
    top = cachedattr('_top')
    bottom = cachedattr('_bottom')

    @property
    @cached
    def width(self):
        return (self.left +
                (self.child.width if self.child else 0) +
                self.right)

    @property
    @cached
    def height(self):
        return (self.bottom +
                (self.child.height if self.child else 0) +
                self.top)

    def _reposition(self, x: int = 0, y: int = 0):
        if self.child:
            self.child.reposition(x=x + self.left, y=y + self.bottom)
