"""UI windowing elements."""
from __future__ import annotations

from enum import Enum, auto
from functools import wraps
import typing as T
import logging

import pyglet
from pyglet import gl

from ..resource import textures
from ..exceptions import UIException

logger = logging.getLogger(__name__)

_EventType = T.Optional[bool]


class Direction(Enum):
    CENTER = auto()
    NORTH = auto()
    NORTHEAST = auto()
    EAST = auto()
    SOUTHEAST = auto()
    SOUTH = auto()
    SOUTHWEST = auto()
    WEST = auto()
    NORTHWEST = auto()


class Axis(Enum):
    HORIZONTAL = auto()
    VERTICAL = auto()

    def __invert__(self) -> Axis:
        return {
            Axis.HORIZONTAL: Axis.VERTICAL,
            Axis.VERTICAL: Axis.HORIZONTAL,
        }[self]


class Alignment(Enum):
    START = auto()
    CENTER = auto()
    END = auto()

    def offset(self, space: int, size: int) -> int:
        """Align something with size `size` in a space of size `space`."""
        return {
            Alignment.START: 0,
            Alignment.CENTER: (space - size) // 2,
            Alignment.END: space - size,
        }[self]

    def __invert__(self) -> Alignment:
        return {
            Alignment.START: Alignment.END,
            Alignment.CENTER: Alignment.CENTER,
            Alignment.END: Alignment.START,
        }[self]


class UITexture(Enum):
    SELECTOR = 0
    CURSOR = 1
    TIMESCALE_PAUSE = 2
    TIMESCALE_SLOW = 3
    TIMESCALE_MEDIUM = 4
    TIMESCALE_FAST = 5
    TIMESCALE_FREERUN = 6
    OXYGEN = 7
    X = 8
    IMPORTANT = 9
    LASER = 10
    VOLTAGE = 11

    @property
    def texture(self) -> pyglet.graphics.Texture:
        return textures.UI[self.value]


def cached(fn: T.Callable, key: T.Optional[str] = None) -> T.Callable:
    """Cache rendering parameters.

    Most commonly, component dimensions should be cached. Components
    keep track of dependencies, i.e. other components whose dimensions
    depend on its own. When a component's dimensions change, it
    invalidates its cache. It then signals to its dependencies to
    invalidate their own caches, and they signal their dependencies,
    etc etc. This way, we selectively excise the caches only on the
    path from the windowing root to the changed component.
    """
    key = key or fn.__name__

    @wraps(fn)
    def wrapped(self, *args, **kwargs):
        if key not in self._cache:
            self._cache[key] = fn(self, *args, **kwargs)
        return self._cache[key]

    return wrapped


def cachedattr(basename: str, docstring=None):
    """Shortcut to define a cached property with a setter that invalidates the cache."""

    def getter(self):
        return getattr(self, basename)

    def setter(self, value):
        setattr(self, basename, value)
        self.invalidate()

    return property(cached(getter, key=basename), setter, doc=docstring)


def iter_children(component: WindowComponent) -> T.Iterable[WindowComponent]:
    """Iterate over the children of the given component, if any."""
    if hasattr(component, 'child'):
        if component.child is not None:
            yield component.child
    if hasattr(component, 'iter_children'):
        yield from component.iter_children()


class WindowComponent(pyglet.event.EventDispatcher):
    """Abstract base class for a UI element."""
    _debug_color = (255, 0, 0, 255)
    _window = None

    def __init__(self, register_mouse_events: bool = False):
        self.register_event_type('on_close')
        self._depends = set()
        self._position = (0, 0)
        self._cache = {}
        self.invalidate()

        if register_mouse_events:
            self.register_mouse_events()

    def _get_window(self) -> T.Optional[pyglet.window.Window]:
        if self._window is not None:
            return self._window
        else:
            for parent in self._depends:
                window = parent._get_window()
                if window is not None:
                    return window

    @property
    def window(self) -> pyglet.window.Window:
        window = self._get_window()
        if window is not None:
            return window
        else:
            raise UIException(f"No window available for {self}.")

    @property
    def width(self) -> int:
        raise NotImplementedError("Implemented by derived type")

    @property
    def height(self) -> int:
        raise NotImplementedError("Implemented by derived type")

    def draw(self):
        raise NotImplementedError("Implemented by derived type")

    def invalidate(self):
        """Invalidate the render cache.

        This will also invalidate the render cache of all components
        that depend on this. Be advised that rebuilding the cache may
        in some cases be an expensive operation.

        Windowing components are usually able to determine when an
        invalidating change has been made. In such cases, this method
        will be called automatically. You only need to worry about
        calling this manually when making manual changes outside the
        windowing system that require the cache to be rebuilt.
        """
        # invalidation does nothing before cache is created during initialization
        if hasattr(self, '_cache'):
            self._cache = {}
            if len(self._depends) > 0:
                for component in self._depends:
                    component.invalidate()
            else:
                # component tree root
                self.reposition(*self._position)

    def _reposition(self, x: int = 0, y: int = 0):
        pass  # Implemented by derived type

    def reposition(self, x: int = 0, y: int = 0):
        """Compute positions recursively down this component tree.

        The position of components is stored as an attribute
        (`_position`). This method performs some bookkeeping to keep
        `_position` valid, therefore, care must be taken that this
        bookkeeping works as expected when extending this
        class. Derived types may need to define how they reposition
        their elements; in such cases, they should override
        ``WindowComponent._reposition`` instead of this method.
        """
        self._position = (x, y)
        self._reposition(x=x, y=y)

    @property
    def _bounding_box(self) -> T.Tuple[int, int, int, int]:
        x, y = self._position
        return x, y, x + self.width, y + self.height

    def draw_debug(self):
        """Draw the bounding box for this component."""
        x1, y1, x2, y2 = self._bounding_box
        pyglet.graphics.draw(
            4, gl.GL_TRIANGLE_FAN,
            ('v2i', (x1, y1, x2, y1, x2, y2, x1, y2)),
            ('c4B', self._debug_color * 4)
        )

    @classmethod
    def register_window(cls, window: pyglet.window.Window):
        """Register an application window for all components of this type that don't explicitly set their window."""
        cls._window = window

    def register_mouse_events(self, window: T.Optional[pyglet.window.Window] = None):
        """Register handlers for mouse events in the given window."""
        self.register_handlers(
            window or self.window,
            on_mouse_press=self._on_mouse_press_window,
            on_mouse_motion=self._on_mouse_motion_window,
        )

    def unregister_mouse_events(self, window: T.Optional[pyglet.window.Window] = None):
        window = window or self.window
        window.remove_handlers(
            on_mouse_press=self._on_mouse_press_window,
            on_mouse_motion=self._on_mouse_motion_window
        )

    def register_handlers(self, dispatcher: pyglet.event.EventDispatcher, **kwargs: T.Mapping[str, T.Callable]):
        if not hasattr(self, '_handler_registry'):
            self._handler_registry = []
        dispatcher.push_handlers(**kwargs)
        self._handler_registry.append((dispatcher, kwargs))

    def close(self):
        if hasattr(self, '_handler_registry'):
            while len(self._handler_registry) > 0:
                dispatcher, kwargs = self._handler_registry.pop()
                dispatcher.remove_handlers(**kwargs)

        for child in iter_children(self):
            child.close()
        self.dispatch_event('on_close', self)

    def on_mouse_press(self, buttons: int, modifiers: int) -> bool:
        """A mouse button was pressed within this component."""
        logger.debug(f"Clicked through {self}.")
        return False  # Implemented by derived type.

    def _on_mouse_press_window(self, x: int, y: int, buttons: int, modifiers: int) -> _EventType:
        x1, y1, x2, y2 = self._bounding_box
        if x1 <= x < x2 and y1 <= y < y2:
            if self.on_mouse_press(buttons, modifiers):
                return pyglet.event.EVENT_HANDLED

        return pyglet.event.EVENT_UNHANDLED

    def on_mouse_enter(self):
        """The mouse was moved over this component."""
        pass  # Implemented by derived type.

    def on_mouse_leave(self):
        """The mouse was moved away from this component."""
        pass  # Implemented by derived type.

    def _on_mouse_motion_window(self, x: int, y: int, dx: int, dy: int):
        old_x = x - dx
        old_y = y - dy
        x1, y1, x2, y2 = self._bounding_box
        in_bounds = x1 <= x < x2 and y1 <= y < y2
        old_in_bounds = x1 <= old_x < x2 and y1 <= old_y < y2

        if in_bounds and not old_in_bounds:
            self.on_mouse_enter()
        elif old_in_bounds and not in_bounds:
            self.on_mouse_leave()


class Unbounded:
    """Mixin for windowing components that don't have a tangible width or height."""
    @property
    def width(self) -> int:
        return -1

    @width.setter
    def width(self, new: int):
        pass

    @property
    def height(self) -> int:
        return -1

    @height.setter
    def height(self, new: int):
        pass

    @property
    def x(self) -> int:
        return -1

    @x.setter
    def x(self, new: int):
        pass

    @property
    def y(self) -> int:
        return -1

    @y.setter
    def y(self, new: int):
        pass


from . import layout, container, widget, app, menu  # noqa E402

__all__ = [
    'Direction',
    'Alignment',
    'Axis',
    'WindowComponent',
    'layout',
    'container',
    'widget',
    'app',
    'menu',
]
