"""Miscellaneous windowing widgets."""
# TODO this module should be refactored.
from __future__ import annotations

import typing as T

import pyglet

from . import WindowComponent, Unbounded, Axis, layout, cachedattr

if T.TYPE_CHECKING:
    from ...application import Application

try:
    import psutil
    import os
    proc = psutil.Process(os.getpid())

    def memory_info() -> str:
        usage = proc.memory_info().rss
        for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB']:
            if usage < 1024:
                return f"{usage:.2f} {unit}"
            else:
                usage /= 1024
except ImportError:
    # memory profiling unavailable
    def memory_info() -> str:
        return "unknown"


class FPSDisplay(Unbounded, WindowComponent):
    """Displays the current framerate."""

    def __init__(self, application: Application):
        self.application = application
        self.fps_display = pyglet.window.FPSDisplay(application.window)
        super().__init__()

    def draw(self):
        self.fps_display.draw()


class Label(WindowComponent):
    """A text label.

    Supports some HTML markup. See ``pyglet.text.formats.html`` for
    details.
    """
    def __init__(self, anchor_x: str = 'left', anchor_y: str = 'baseline', **kwargs):
        self.label = pyglet.text.HTMLLabel(anchor_x=anchor_x, anchor_y=anchor_y, **kwargs)
        super().__init__()

    @property
    def width(self) -> int:
        return self.label.content_width

    @property
    def height(self) -> int:
        return self.label.content_height

    @property
    def _position(self) -> T.Tuple[int, int]:
        return self.label.x, self.label.y

    @_position.setter
    def _position(self, value: T.Tuple[int, int]):
        self.label.x, self.label.y = value

    @property
    def text(self) -> str:
        return self.label.text

    @text.setter
    def text(self, value: str):
        self.label.text = value
        self.invalidate()

    def _reposition(self, x: int = 0, y: int = 0):
        self.label.x = x
        self.label.y = y

    def draw(self):
        self.label.draw()


class FormatLabel(Label):
    def __init__(self,
                 fmt: str = '<font face="Arial" color="white">{text}</font>',
                 **kwargs):
        self._fmt = fmt
        super().__init__(**kwargs)
        self.text = kwargs.get('text', "")

    @property
    def raw_text(self) -> str:
        return self.label.text

    @property
    def text(self) -> str:
        return self._base_text

    @text.setter
    def text(self, value: str):
        self._base_text = value
        super(FormatLabel, type(self)).text.fset(self, self._fmt.format(text=value))


class MemoryMonitor(FormatLabel):
    """A label displaying the application's memory footprint."""
    def __init__(self,
                 interval_s: float = 5,
                 fmt: str = '<font face="Arial" color="white" size="1"><b>{text}</b></font>',
                 **kwargs):
        pyglet.clock.schedule_interval(self._update, interval_s)
        super().__init__(fmt=fmt, **kwargs)
        self._update()

    def _update(self, dt: int = 0):
        self.text = f"mem: {memory_info()}"


class Image(WindowComponent):
    """A container for a bitmap texture."""
    def __init__(self, texture: pyglet.graphics.Texture,
                 width: int = None,
                 height: int = None,
                 scale: float = None,
                 scale_x: float = None,
                 scale_y: float = None,
                 color: T.Tuple[int, int, int] = None,
                 opacity: int = None,
                 rotation: int = None,
                 visible: bool = True,
                 register_mouse_events: bool = False,
                 **kwargs):
        self.sprite = pyglet.sprite.Sprite(texture, **kwargs)
        if width is not None:
            self.width = width
        if height is not None:
            self.height = height
        if scale is not None:
            self.scale = scale
        if scale_x is not None:
            self.scale_x = scale_x
        if scale_y is not None:
            self.scale_y = scale_y
        if color is not None:
            self.color = color
        if opacity is not None:
            self.opacity = opacity
        if rotation is not None:
            self.rotation = rotation
        self.visible = visible
        super().__init__(register_mouse_events=register_mouse_events)

    @property
    def _position(self) -> T.Tuple[int, int]:
        return self.sprite.position

    @_position.setter
    def _position(self, value: T.Tuple[int, int]):
        self.sprite.position = value

    @property
    def y(self) -> int:
        return self.sprite.y

    @y.setter
    def y(self, value: int):
        self.sprite.y = value
        self.invalidate()

    @property
    def width(self) -> int:
        return self.sprite.width

    @width.setter
    def width(self, value: int):
        self.sprite.scale_x = 1.0
        self.sprite.scale_x = value / self.sprite.width
        self.invalidate()

    @property
    def height(self) -> int:
        return self.sprite.height

    @height.setter
    def height(self, value: int):
        self.sprite.scale_y = 1.0
        self.sprite.scale_y = value / self.sprite.height
        self.invalidate()

    @property
    def scale(self) -> float:
        return self.sprite.scale

    @scale.setter
    def scale(self, value: float):
        self.sprite.scale = value
        self.invalidate()

    @property
    def scale_x(self) -> float:
        return self.sprite.scale_x

    @scale_x.setter
    def scale_x(self, value: float):
        self.sprite.scale_x = value
        self.invalidate()

    @property
    def scale_y(self) -> float:
        return self.sprite.scale_y

    @scale_y.setter
    def scale_y(self, value: float):
        self.sprite.scale_y = value
        self.invalidate()

    @property
    def color(self) -> T.Tuple[int, int, int]:
        return self.sprite.color

    @color.setter
    def color(self, value: T.Tuple[int, int, int]):
        self.sprite.color = value

    @property
    def opacity(self) -> int:
        return self.sprite.opacity

    @opacity.setter
    def opacity(self, value: int):
        self.sprite.opacity = value

    @property
    def rotation(self) -> float:
        return self.sprite.rotation

    @rotation.setter
    def rotation(self, value: float):
        self.sprite.rotation = value

    @property
    def visible(self) -> bool:
        return self.sprite.visible

    @visible.setter
    def visible(self, value: bool):
        self.sprite.visible = value

    def _reposition(self, x: int = 0, y: int = 0):
        self.sprite.update(x=x, y=y)

    def draw(self):
        # TODO batching
        self.sprite.draw()


class ToggleOverlay(layout.Stack):
    """A widget with two layers: a static background and a toggleable overlay.

    For best results, ensure that the background and foreground
    components are of the same size.
    """
    def __init__(self, background: WindowComponent, overlay: WindowComponent, **kwargs):
        super().__init__(background, **kwargs)
        self.background = background
        self.overlay = overlay

    @property
    def show_overlay(self) -> bool:
        return self.overlay in self

    @show_overlay.setter
    def show_overlay(self, value: bool):
        if value:
            if self.overlay not in self:
                self.append(self.overlay)
        else:
            if self.overlay in self:
                self.remove(self.overlay)


class MessageQueue(layout.FixedAlign):
    """A rotating queue that displays its children in order."""
    def __init__(self, *args, limit: int, axis: Axis = Axis.VERTICAL, **kwargs):
        self._limit = limit
        super().__init__(axis=axis, **kwargs)

    limit = cachedattr('_limit')

    def append(self, *args, **kwargs):
        super().append(*args, **kwargs)
        if len(self) > self.limit:
            self.pop(0)
