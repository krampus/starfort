"""Application-specific UI elements"""
from __future__ import annotations

import typing as T
import logging

from pyglet.window import mouse

from . import widget, layout, Axis, Alignment, UITexture
from ..resource import fonts
from ..game.world import Timescale
from ..game import entities

if T.TYPE_CHECKING:
    from ..application import Application

logger = logging.getLogger(__name__)

_INACTIVE_OPACITY = 150
_ACTIVE_OPACITY = 255


def on_left_click(func: T.Callable):
    def on_mouse_press(button: int, modifiers: int):
        if button == mouse.LEFT:
            func()
            return True
    return on_mouse_press


class TimescaleControl(layout.Align):
    """Widget for controlling the application timescale"""
    def __init__(self, application: Application):

        def toggle_pause():
            application.world.paused = not application.world.paused

        def timescale_setter(timescale: Timescale):
            def set_timescale():
                application.world.timescale = timescale
            return set_timescale

        self._pause = widget.Image(UITexture.TIMESCALE_PAUSE.texture, opacity=_INACTIVE_OPACITY,
                                   register_mouse_events=True)
        self._pause.on_mouse_press = on_left_click(toggle_pause)
        self._slow = widget.Image(UITexture.TIMESCALE_SLOW.texture, opacity=_INACTIVE_OPACITY,
                                  register_mouse_events=True)
        self._slow.on_mouse_press = on_left_click(timescale_setter(Timescale.SLOW))
        self._medium = widget.Image(UITexture.TIMESCALE_MEDIUM.texture, opacity=_INACTIVE_OPACITY,
                                    register_mouse_events=True)
        self._medium.on_mouse_press = on_left_click(timescale_setter(Timescale.MEDIUM))
        self._fast = widget.Image(UITexture.TIMESCALE_FAST.texture, opacity=_INACTIVE_OPACITY,
                                  register_mouse_events=True)
        self._fast.on_mouse_press = on_left_click(timescale_setter(Timescale.FAST))
        self._freerun = widget.Image(UITexture.TIMESCALE_FREERUN.texture, opacity=_INACTIVE_OPACITY,
                                     register_mouse_events=True)
        self._freerun.on_mouse_press = on_left_click(timescale_setter(Timescale.FREERUN))

        super().__init__(
            self._pause,
            self._slow,
            self._medium,
            self._fast,
            self._freerun,
            axis=Axis.HORIZONTAL
        )

        self.register_handlers(
            application.world,
            on_set_pause_state=self.on_set_pause_state,
            on_set_timescale=self.on_set_timescale
        )

    def on_set_pause_state(self, paused: bool):
        if paused:
            self._pause.opacity = _ACTIVE_OPACITY
            self._pause.color = (255, 0, 0)
        else:
            self._pause.opacity = _INACTIVE_OPACITY
            self._pause.color = (255, 255, 255)

    def on_set_timescale(self, timescale: Timescale):
        inactive_states = {
            Timescale.SLOW: self._slow,
            Timescale.MEDIUM: self._medium,
            Timescale.FAST: self._fast,
            Timescale.FREERUN: self._freerun
        }
        active_state = inactive_states.pop(timescale)
        active_state.opacity = _ACTIVE_OPACITY
        for state in inactive_states.values():
            state.opacity = _INACTIVE_OPACITY


class OverlayControl(layout.Align):
    """Widget for controlling which world overlays are drawn"""

    def __init__(self, application: Application):
        def toggle_atmosphere_overlay():
            application.world.draw_atmosphere_overlay = not application.world.draw_atmosphere_overlay

        self._atmosphere_overlay = widget.ToggleOverlay(
            widget.Image(UITexture.OXYGEN.texture, scale=0.5),
            widget.Image(UITexture.SELECTOR.texture, scale=0.5),
            register_mouse_events=True
        )

        super().__init__(
            self._atmosphere_overlay,
            axis=Axis.HORIZONTAL
        )

        self._atmosphere_overlay.on_mouse_press = on_left_click(toggle_atmosphere_overlay)
        self.on_set_atmosphere_overlay_display(application.world.draw_atmosphere_overlay)
        self.register_handlers(
            application.world,
            on_set_atmosphere_overlay_display=self.on_set_atmosphere_overlay_display
        )

    def on_set_atmosphere_overlay_display(self, value: bool):
        self._atmosphere_overlay.show_overlay = value


class WorldCursorInfo(layout.FixedAlign):
    """Widget showing info about the tile under the world cursor"""
    def __init__(self, application: Application):
        self.world = application.world
        self._tile_info = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="gray" size="3">{{text}}</font>',
        )
        self._location_info = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="gray" size="2">{{text}}</font>',
        )
        self._atmosphere_info = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="gray" size="2">{{text}}</font>',
        )

        super().__init__(
            self._tile_info,
            self._location_info,
            self._atmosphere_info,
            space=15,
            axis=Axis.VERTICAL,
            align=Alignment.START
        )
        self.register_handlers(
            self.world,
            on_set_cursor=self.on_set_cursor,
            on_atmosphere_update=self.on_atmosphere_update
        )

    def on_set_cursor(self, cursor: T.Optional[T.Tuple[int, int]]):
        if cursor is not None:
            self._location_info.text = str(cursor)
            cursor_tile = self.world.get_tile()
            if cursor_tile is not None:
                self._tile_info.text = cursor_tile.display_name
            else:
                self._tile_info.text = "out of range"
        else:
            self._location_info.text = ""
            self._tile_info.text = ""
        self.on_atmosphere_update()

    def on_atmosphere_update(self):
        if self.world.cursor is not None:
            if self.world.cursor in self.world:
                value = self.world.atmosphere[(self.world.active_layer, *self.world.cursor)] * 100
            else:
                value = 0
            # TODO i18n
            self._atmosphere_info.text = f"Atmospheric pressure: {value:.2f}%"
        else:
            self._atmosphere_info.text = ""


class SelectionInfo(layout.FixedAlign):
    """Widget showing info about the current selection."""
    def __init__(self, application: Application):
        self._header = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="green", size="3">{{text}}</font>'
        )
        self._entity_world_info = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="green", size="3">{{text}}</font>'
        )
        self._entity_task_info = widget.FormatLabel(
            fmt=f'<font face="{fonts.MONITOR}" color="green", size="3">{{text}}</font>'
        )

        super().__init__(
            self._entity_task_info,
            self._entity_world_info,
            self._header,
            space=20,
            axis=Axis.VERTICAL,
            align=Alignment.END
        )

        # Initialize labels to no selection
        self._selected = tuple()
        self.on_update_selection(tuple())
        self.register_handlers(
            application.selection,
            on_update_selection=self.on_update_selection
        )

    def on_move(self, z: float, x: float, y: float):
        self._entity_world_info.text = f"World location: {(int(z), int(x), int(y))}"

    def on_set_behavior(self, behavior: entities.behaviors.Behavior):
        self._entity_task_info.text = f"Current task: {behavior.display_noun}"

    def on_update_selection(self, selection: T.Tuple[entities.Entity]):
        for e in self._selected:
            e.remove_handler('on_move', self.on_move)
            e.remove_handler('on_set_behavior', self.on_set_behavior)

        self._selected = selection

        # TODO i18n
        if len(selection) == 0:
            self._entity_task_info.text = self._entity_world_info.text = ""
            self._header.text = "No selection."
        elif len(selection) > 1:
            self._entity_task_info.text = self._entity_world_info.text = ""
            self._header.text = f"Selected {len(selection)} things."
        else:
            e = selection[0]
            self._header.text = f"Selected {e.display_name}."
            handlers = {'on_move': self.on_move}
            self.on_move(e.z, e.x, e.y)
            if isinstance(e, entities.actors.Actor):
                self.on_set_behavior(e.behavior)
                handlers['on_set_behavior'] = self.on_set_behavior
            else:
                self._entity_task_info.text = ""
            self.register_handlers(e, **handlers)


class LayerInfo(widget.FormatLabel):
    """Widget showing info about the current active layer"""
    def __init__(self, application: Application):
        super().__init__(
            fmt=f'<font face="{fonts.MONITOR}" color="white">{{text}}</font>',
        )
        self.on_set_active_layer(application.world.active_layer)
        self.register_handlers(application.world, on_set_active_layer=self.on_set_active_layer)

    def on_set_active_layer(self, z: int):
        self.text = f"layer {z}"
