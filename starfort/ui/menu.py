"""Context menus."""
from __future__ import annotations

import typing as T
import logging

import pyglet
from pyglet.window import mouse

from . import layout, container, widget, Axis, Alignment, UITexture, iter_children
from ..resource import fonts

if T.TYPE_CHECKING:
    from . import WindowComponent

logger = logging.getLogger(__name__)

_DEFAULT_ITEM_HEIGHT = 20

# TODO framework for UI chrome
_BG_INACTIVE_COLOR = (0.4, 0.8, 0.8, 0.5)
_BG_ACTIVE_COLOR = (0.4, 0.8, 0.8, 0.8)
_BG_DISABLED_COLOR = (0.3, 0.3, 0.3, 0.8)
_BORDER_COLOR = (0.4, 1.0, 1.0, 0.8)
_WHITE = (1.0, 1.0, 1.0, 1.0)


def _submenu_arrow_icon(width: int = 12, height: int = 12):
    # XXX this really should be a distinct UI element.
    img = widget.Image(UITexture.TIMESCALE_SLOW.texture, scale=0.5)
    h_pad = (img.width - width) // 2
    v_pad = (img.height - height) // 2
    return container.Fixed(
        child=container.Margin(
            child=img,
            horizontal=-h_pad,
            vertical=-v_pad,
        ),
        width=width, height=height
    )


def make_menu_item_label(text: T.Optional[str] = None):
    return widget.FormatLabel(
        text=text if text is not None else "",
        fmt=f'<font face="{fonts.MONITOR}" color="white" size="2.5">{{text}}</font>'
    )


class MenuItem(container.Fixed):
    """An item in a context menu."""
    def __init__(self, label: T.Union[str, widget.Label],
                 callback: T.Optional[T.Callable],
                 icon: T.Optional[widget.Image] = None,
                 close_on_callback: bool = True,
                 axis: Axis = Axis.HORIZONTAL,
                 align: Alignment = Alignment.START,
                 width: T.Optional[int] = None,
                 height: T.Optional[int] = _DEFAULT_ITEM_HEIGHT,
                 padding: T.Optional[dict] = None,
                 parent: T.Optional[Menu] = None,
                 **kwargs):
        self.callback = callback
        self.close_on_callback = close_on_callback
        self.parent = parent

        if isinstance(label, str):
            label = make_menu_item_label(label)

        self._label = label

        height = height if height is not None else label.height

        child = container.Margin(
            child=layout.Align(
                container.Fixed(
                    child=icon,
                    width=height,
                    height=height
                ),
                label,
                axis=axis,
                align=align
            ),
            **(padding or {'margin': 4})
        )

        super().__init__(
            child=child,
            width=width if width is not None else child.width,
            height=height,
            register_mouse_events=True,
            **kwargs)

        self.bg_color = _BG_INACTIVE_COLOR

    @property
    def text(self) -> str:
        return self._label.text

    @text.setter
    def text(self, value: str):
        self._label.text = value

    def on_mouse_press(self, button: int, modifiers: int) -> bool:
        if button == mouse.LEFT:
            if self.callback is not None:
                self.callback()
                if self.close_on_callback and self.parent is not None:
                    self.parent.close()
            return True
        else:
            super().on_mouse_press(button, modifiers)

    def on_mouse_enter(self):
        self.bg_color = _BG_ACTIVE_COLOR

    def on_mouse_leave(self):
        self.bg_color = _BG_INACTIVE_COLOR

    def _reposition(self, **kwargs):
        x1, y1 = self._position
        x2, y2 = x1 + self.width, y1 + self.height
        self._vlist = pyglet.graphics.vertex_list(
            4,
            ('v2f', (x1, y1, x2, y1, x2, y2, x1, y2))
        )
        super()._reposition(**kwargs)

    def draw(self):
        # TODO framework for UI chrome
        pyglet.gl.glColor4f(*self.bg_color)
        self._vlist.draw(pyglet.gl.GL_QUADS)
        pyglet.gl.glColor4f(*_BORDER_COLOR)
        self._vlist.draw(pyglet.gl.GL_LINE_LOOP)
        pyglet.gl.glColor4f(*_WHITE)
        super().draw()


class SubMenuKeyItem(layout.Stack):
    def __init__(self, label: T.Union[str, widget.Label],
                 submenu: Menu,
                 parent: Menu,
                 icon: T.Optional[widget.Image] = None,
                 **kwargs):
        self.submenu = submenu
        self._open = False
        self.item = MenuItem(label, self._toggle_submenu, icon=icon,
                             close_on_callback=False, parent=parent, **kwargs)
        super().__init__(
            self.item,
            _submenu_arrow_icon(width=12, height=12),
            h_align=Alignment.END,
            v_align=Alignment.CENTER
        )

    @layout.Stack.width.setter
    def width(self, value: int):
        self.item.width = value

    @layout.Stack.height.setter
    def height(self, value: int):
        self.item.height = value

    def _toggle_submenu(self):
        if self._open:
            self.item.parent.close_submenus()
        else:
            self.item.parent.open_submenu(self, self.submenu)
        self._open = not self._open

    def close(self):
        super().close()
        self.submenu.close()


class DisabledMenuItem(MenuItem):
    def __init__(self, label: T.Union[str, widget.Label], **kwargs):
        super().__init__(label, None, close_on_callback=False, **kwargs)
        self.bg_color = _BG_DISABLED_COLOR

    def on_mouse_enter(self):
        pass

    def on_mouse_leave(self):
        pass


_DeclarativeMenu = T.Union[
    T.Tuple[T.Union[str, widget.Label], T.Optional[T.Callable]],
    T.Tuple[T.Optional[widget.Image], T.Union[str, widget.Label], T.Optional[T.Callable]],
    MenuItem
]


class Menu(layout.Align):
    """A context menu with a number of selectable options."""
    def __init__(self,
                 *items: _DeclarativeMenu,
                 axis: Axis = Axis.VERTICAL,
                 item_width: T.Optional[int] = None,
                 item_height: T.Optional[int] = _DEFAULT_ITEM_HEIGHT,
                 **kwargs):

        minor_axis = axis
        major_axis = ~axis

        def declarative_menu(item: _DeclarativeMenu) -> MenuItem:
            if isinstance(item, MenuItem):
                item.parent = self
                return item
            else:
                if len(item) == 2:
                    icon = None
                    label, callback = item
                elif len(item) == 3:
                    icon, label, callback = item
                else:
                    raise ValueError(f"Can't build a menu item from {item}.")

                if callback is None:
                    return DisabledMenuItem(label, icon=icon,
                                            width=item_width, height=item_height, parent=self)
                if isinstance(callback, T.Iterable):
                    callback = Menu(*callback, axis=axis,
                                    item_width=item_width, item_height=item_height, **kwargs)
                if isinstance(callback, Menu):
                    self.register_handlers(callback, on_close=lambda m: self.close())
                    return SubMenuKeyItem(label, callback, parent=self,
                                          icon=icon, width=item_width, height=item_height)
                else:
                    return MenuItem(label, callback, icon=icon,
                                    width=item_width, height=item_height, parent=self)

        items = [declarative_menu(i) for i in items]
        if minor_axis == Axis.VERTICAL:
            # reverse items for vertical menus
            items = items[::-1]

        # all items should have uniform width
        if len(items) > 0:
            item_width = item_width if item_width is not None else max(i.width for i in items)
            for i in items:
                i.width = item_width

        super().__init__(
            layout.Align(
                *items,
                axis=minor_axis,
                **kwargs
            ),
            axis=major_axis,
            align=Alignment.END
            )

    def iter_items(self):
        yield from self[0].iter_children()

    def reposition(self, x: int = 0, y: int = 0):
        # compensate for menu height, so menu "drops down" instead of up
        super().reposition(x=x, y=y - self.height)

    def open_submenu(self, key: MenuItem, menu: Menu):
        logger.debug(f"Opening submenu {menu}")
        self.close_submenus()
        key_idx = self[0].children.index(key)
        if self.axis == Axis.HORIZONTAL:
            margins = {'top': sum(c.height for c in self[0].children[key_idx + 1:]) - menu.height}
        else:
            margins = {'left': sum(c.width for c in self[0].children[key_idx:])}
        self.append(
            container.Margin(
                child=menu,
                **margins
            )
        )

    def close_submenus(self):
        while len(self) > 1:
            margin = self.pop(1)
            margin.reposition(-1, -1)
