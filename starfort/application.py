"""Application logic"""
from __future__ import annotations

from dataclasses import dataclass
from contextlib import contextmanager
import typing as T
import logging

import pyglet
from pyglet import gl
from pyglet.window import mouse, key

from . import __version__, ui, eventloop, debug
from .config import appconfig
from .game import world, entities, grid_to_screen, screen_to_grid
from .resource import fonts, textures

logger = logging.getLogger(__name__)

# TODO i18n
_WINDOW_TITLE = f"starfort {__version__}"

_ZOOM_SCALE_FACTOR = 1/2


def gl_init():
    """Initialize our OpenGL pipeline.

    This only needs to be called once, before any rendering is done
    but after the application window is created.
    """
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
    fonts.load()


# XXX should maybe just use MVM directly
@dataclass
class Camera:
    """Translate between window-space and virtual screen-space.

    This works like a high-level interface to the GL modelview matrix
    in two dimensions. Conceptually, the game is rendered in a big
    flat plane that extends beyond the bounds of the window, which
    we'll call virtual screen-space. The ``Camera`` allows us to
    easily control how window-space maps to screen-space by "panning"
    and "zooming" a camera, and lets us translate window-space
    interactions (such as user clicks) to screen-space.
    """

    window: pyglet.window.Window
    x: float = 0.0
    y: float = 0.0
    z: float = 1.0
    x_bounds: T.Optional[T.Tuple[float, float]] = (-3000, 0)
    y_bounds: T.Optional[T.Tuple[float, float]] = (-1000, 1000)
    z_bounds: T.Optional[T.Tuple[float, float]] = (1.0, 6.0)

    def pan(self, dx: float, dy: float):
        """Pan the camera in the cardinal directions"""
        self.x = max(self.x_bounds[0], min(self.x_bounds[1], self.x + dx / self.z))
        self.y = max(self.y_bounds[0], min(self.y_bounds[1], self.y + dy / self.z))

    def zoom(self, dzoom: float):
        """Zoom the camera in and out."""
        self.z = max(self.z_bounds[0], min(self.z_bounds[1], self.z + dzoom))

    def gl_set_matrix(self):
        """Translate & scale the view matrix to the camera's position."""
        gl.glTranslatef(self.window.width / 2, self.window.height / 2, 0.0)
        gl.glScalef(self.z, self.z, 1.0)
        gl.glTranslatef(self.x, self.y, 0.0)

    def window_to_screen(self, x: int, y: int) -> T.Tuple[float, float]:
        """Translate a point in window-space (e.g. a user click) to virtual screen-space"""
        return (x - self.window.width / 2) / self.z - self.x, (y - self.window.height / 2) / self.z - self.y

    def screen_to_window(self, x: int, y: int) -> T.Tuple[int, int]:
        """Translate coordinates from virtual screen-space to window-space"""
        return (x + self.x) * self.z + self.window.width / 2, (y + self.y) * self.z + self.window.height / 2


@dataclass
class Selection(pyglet.event.EventDispatcher):
    application: Application
    _start: T.Optional[T.Tuple[int, int]] = None
    _end: T.Optional[T.Tuple[int, int]] = None

    def __post_init__(self):
        self._entities = tuple()
        self.register_event_type('on_update_selection')

    @property
    def start(self) -> T.Optional[T.Tuple[int, int]]:
        return self._start

    @start.setter
    def start(self, value: T.Optional[T.Tuple[int, int]]):
        self._start = self.end = value
        self.entities = tuple()

    @property
    def end(self) -> T.Optional[T.Tuple[int, int]]:
        return self._end

    @end.setter
    def end(self, value: T.Optional[T.Tuple[int, int]]):
        self._end = value
        self.entities = tuple(self._iter_entities_in_region())

    @property
    def entities(self) -> T.Tuple[entities.Entity]:
        return self._entities

    @entities.setter
    def entities(self, value: T.Tuple[entities.Entity]):
        if value != self._entities:
            for e in self._entities:
                if e not in value:
                    e.remove_handler('on_remove', self.on_remove)
            for e in value:
                if e not in self._entities:
                    e.event(self.on_remove)

            self._entities = value
            self.dispatch_event('on_update_selection', value)

    def finish(self):
        self._start = self._end = None

    @property
    def active(self) -> bool:
        return self.start is not None

    def on_remove(self, entity: entities.Entity):
        self.entities = tuple(e for e in self.entities if e is not entity)

    def on_update_selection(self, value: T.Tuple[entities.Entity]):
        logger.debug(f"Selected {len(value)} entities.")

    def _iter_entities_in_region(self) -> T.Iterable[entities.Entity]:
        if self.active:
            grid_start = (self.application.world.active_layer,) + screen_to_grid(*self.start)
            grid_end = (self.application.world.active_layer,) + screen_to_grid(*self.end)
            yield from self.application.world.entities_in_region(grid_start, grid_end)

    @property
    def vertices(self) -> T.Iterable[int]:
        if self.active:
            ax, ay = self.start
            bx, by = self.end

            # relies on fixed pseudoisometric perspective: width / height = 2
            return (
                ax, ay,
                (ax + bx) / 2 + (ay - by), (ax - bx) / 4 + (ay + by) / 2,
                bx, by,
                (bx + ax) / 2 + (by - ay), (bx - ax) / 4 + (by + ay) / 2,
            )

    def draw(self):
        if self.active:
            gl.glColor4f(0.4, 1.0, 1.0, 0.5)
            verts = self.vertices
            pyglet.graphics.draw(4, gl.GL_QUADS, ('v2f', verts))

        # TODO optimize
        for entity in self.entities:
            if int(entity.z) == self.application.world.active_layer:
                sx, sy = grid_to_screen(entity.x, entity.y)
                textures.UI[0].blit(sx - textures.DEFAULT_ANCHOR_X, sy - textures.DEFAULT_ANCHOR_Y)


def make_debug_menu() -> ui.menu.Menu:
    def callback_a():
        print("Callback A was called!")

    def callback_b():
        print("Callback B was called!")

    icon = ui.widget.Image(textures.UI[8], width=12, height=12)
    submenu_icon = ui.widget.Image(textures.UI[9], width=12, height=12)
    # return ui.menu.Menu(
    #     ("Debug Menu", None),
    #     (icon, "Menu item A", callback_a),
    #     ("submenu...", (
    #         ("submenu item a", callback_a),
    #         (submenu_icon, "This does nothing", None),
    #         ("what is this...", (
    #             ("that's right it's another fuckin' submenu!", None),
    #             ("subsubmenu item a", callback_a),
    #             ("subsubmenu item b", callback_b),
    #             ("what is this...", (
    #                 ("that's right it's another fuckin' submenu!", None),
    #                 ("subsubmenu item a", callback_a),
    #                 ("subsubmenu item b", callback_b),
    #             ))
    #         )),
    #         ("submenu item b", callback_b),
    #     )),
    #     ("And menu item B", callback_b),
    #     item_height=20,
    # )
    return ui.menu.Menu(
        ('submenu a...', (
            ("submenu item a", callback_a),
            (submenu_icon, "This does nothing", None),
            ("what is this...", (
                ("that's right it's another fuckin' submenu!", None),
                ("subsubmenu item a", callback_a),
                ("subsubmenu item b", callback_b),
                ("what is this...", (
                    ("that's right it's another fuckin' submenu!", None),
                    ("subsubmenu item a", callback_a),
                    ("subsubmenu item b", callback_b),
                ))
            )),
            ("submenu item b", callback_b),
        )),
        ('submenu b...', (
            ("submenu item a", callback_a),
            (submenu_icon, "This does nothing", None),
            ("what is this...", (
                ("that's right it's another fuckin' submenu!", None),
                ("subsubmenu item a", callback_a),
                ("subsubmenu item b", callback_b),
                ("what is this...", (
                    ("that's right it's another fuckin' submenu!", None),
                    ("subsubmenu item a", callback_a),
                    ("subsubmenu item b", callback_b),
                ))
            )),
            ("submenu item b", callback_b),
        )),
        ('submenu c...', (
            ("submenu item a", callback_a),
            (submenu_icon, "This does nothing", None),
            ("what is this...", (
                ("that's right it's another fuckin' submenu!", None),
                ("subsubmenu item a", callback_a),
                ("subsubmenu item b", callback_b),
                ("what is this...", (
                    ("that's right it's another fuckin' submenu!", None),
                    ("subsubmenu item a", callback_a),
                    ("subsubmenu item b", callback_b),
                ))
            )),
            ("submenu item b", callback_b),
        )),
        ('submenu d...', (
            ("submenu item a", callback_a),
            (submenu_icon, "This does nothing", None),
            ("what is this...", (
                ("that's right it's another fuckin' submenu!", None),
                ("subsubmenu item a", callback_a),
                ("subsubmenu item b", callback_b),
                ("what is this...", (
                    ("that's right it's another fuckin' submenu!", None),
                    ("subsubmenu item a", callback_a),
                    ("subsubmenu item b", callback_b),
                ))
            )),
            ("submenu item b", callback_b),
        )),
    )


class Application:
    """Container for runtime data"""
    def __init__(self, config: T.Optional[appconfig.AppConfig] = None):
        self.config = config or appconfig.AppConfig()

        self.window = pyglet.window.Window(
            caption=_WINDOW_TITLE,
            width=self.config.window_width,
            height=self.config.window_height,
            fullscreen=self.config.fullscreen,
            vsync=self.config.vsync,
            resizable=not self.config.fullscreen,
            visible=False,
        )
        ui.WindowComponent.register_window(self.window)

        self._drag_state = {
            mouse.LEFT: False,
            mouse.MIDDLE: False,
            mouse.RIGHT: False
        }
        self._press_state = {
            mouse.LEFT: False,
            mouse.MIDDLE: False,
            mouse.RIGHT: False
        }

        gl_init()  # TODO when should this be called?

        self.window.push_handlers(self)

        pyglet.clock.schedule_interval(self.update, 1/self.config.target_fps)

        self.selection = Selection(self)

        self.camera = Camera(self.window)
        self.world = debug.make_world()
        self.world.render()

        self._dialog = []  # TODO

        self._init_decorations()

        self.add_message("Now playing...")
        self.world.paused = True

    def _init_decorations(self):
        self._decoration_base = ui.container.Fixed(
            width=self.window.width,
            height=self.window.height
        )
        self.context_menus = ui.layout.Absolute()

        self.messagebox = ui.widget.MessageQueue(limit=6, space=20)

        self.decoration = ui.layout.Stack(
            dict(
                child=self._decoration_base,
                h_align=ui.Alignment.CENTER,
                v_align=ui.Alignment.CENTER
            ),
            dict(
                child=ui.widget.Label(
                    text=f'<h1><font face="{fonts.TECH}" color="white">{_WINDOW_TITLE}</font></h1>',
                    anchor_y='bottom'
                ),
                h_align=ui.Alignment.START,
                v_align=ui.Alignment.END),
            dict(
                child=ui.container.Margin(
                    child=self.messagebox,
                    margin=10
                ),
                h_align=ui.Alignment.END,
                v_align=ui.Alignment.END
            ),
            dict(
                child=ui.container.Margin(
                    child=ui.app.WorldCursorInfo(self),
                    left=10,
                    bottom=40,
                ),
                h_align=ui.Alignment.START,
                v_align=ui.Alignment.START
            ),
            dict(
                child=ui.container.Margin(
                    child=ui.layout.Align(
                        ui.app.TimescaleControl(self),
                        ui.container.Margin(
                            child=ui.app.OverlayControl(self),
                            top=15
                        ),
                        ui.app.LayerInfo(self),
                        ui.app.SelectionInfo(self),
                        axis=ui.Axis.VERTICAL,
                        align=ui.Alignment.END
                    ),
                    margin=10
                ),
                h_align=ui.Alignment.END,
                v_align=ui.Alignment.START
            ),
            self.context_menus
        )

        if self.config.show_fps:
            self.decoration.append(
                ui.widget.FPSDisplay(self)
            )

        if self.config.debug:
            self.decoration.append(
                ui.container.Margin(
                    child=ui.widget.MemoryMonitor(anchor_y='baseline'),
                    left=120
                ),
                h_align=ui.Alignment.START,
                v_align=ui.Alignment.START
            )

    def add_message(self, message: str):
        self.messagebox.append(ui.widget.Label(
            text=f'<font face="{fonts.MONITOR}" color="white">{message}</font>',
            anchor_y='bottom'
        ), align=ui.Alignment.END)

    @contextmanager
    def _gl_context(self):
        gl.glPushMatrix()

        gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)

        yield

        gl.glPopMatrix()

    def on_draw(self):
        self.window.clear()
        with self._gl_context():
            self.camera.gl_set_matrix()

            self.world.draw()

            if self.config.debug:
                self.world.draw_bounds()

            # draw selection
            self.selection.draw()

        self.decoration.draw()
        for element in self._dialog:
            element.draw()

        if self.config.debug:
            gl.glColor4f(1, 0, 0, 1)
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
            self.decoration.draw_debug()
            for element in self._dialog:
                element.draw_bounds()

    def on_resize(self, width: int, height: int):
        self._decoration_base.width = width
        self._decoration_base.height = height

    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        # if there is a context menu open (and it wasn't clicked-on), close it
        for menu in self.context_menus:
            menu.close()
        self._press_state[button] = True

    def on_click(self, x: int, y: int, button: int, modifiers: int):
        # TODO handle control bindings
        sx, sy = self.camera.window_to_screen(x, y)
        gx, gy = screen_to_grid(sx, sy)
        location = (self.world.active_layer, int(gx), int(gy))

        logger.debug(f"Mouse press: ({mouse.buttons_string(button)}, {modifiers})\n"
                     f"\twindow ({x}, {y})\n"
                     f"\tscreen ({sx:.3f}, {sy:.3f})\n"
                     f"\tgrid [{gx:.3f}, {gy:.3f}]")

        if button == mouse.LEFT:
            if modifiers & key.MOD_CTRL:
                # DEBUG - add a mite
                mite = entities.actors.Mite(*location)
                self.world.add_entity(mite)
                self.add_message(f"Spawned mite @ {mite.location}")
            elif modifiers & key.MOD_SHIFT:
                # DEBUG - add a beacon
                self.add_message(f"Placing beacon @ {location}")
                for e in self.world.iter_entities():
                    if isinstance(e, entities.actors.Mite):
                        e.behavior = entities.behaviors.MoveToTarget(e, *location)
            else:
                # Select entity at location, if any
                first_ent = next(self.world.entities_at_location(location), None)
                selection = (first_ent,) if first_ent is not None else tuple()
                self.selection.entities = selection
        elif button == mouse.RIGHT:
            first_ent = next(self.world.entities_at_location(location), None)
            if first_ent is None:
                menu = debug.make_world_menu(self.world, location)
            else:
                menu = debug.make_entity_menu(first_ent)

            menu.push_handlers(on_close=lambda m: self.context_menus.remove(m) if m in self.context_menus else None)
            self.context_menus.append(menu, x, y)

    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        # TODO handle control bindings
        sx, sy = self.camera.window_to_screen(x, y)
        gx, gy = screen_to_grid(sx, sy)

        if self._drag_state[button]:
            # end of dragging
            if button == mouse.LEFT:
                self.selection.finish()
        elif self._press_state[button]:
            # Create click event
            self.on_click(x, y, button, modifiers)

        self._drag_state[button] = self._press_state[button] = False

    def on_mouse_motion(self, x: int, y: int, dx: float, dy: float):
        gx, gy = screen_to_grid(*self.camera.window_to_screen(x, y))
        self.world.cursor = (int(gx), int(gy))

    def on_mouse_drag(self, x: int, y: int, dx: float, dy: float, buttons: int, modifiers: int):
        # TODO handle control bindings
        if buttons & mouse.LEFT:
            self._drag_state[mouse.LEFT] = True
            if not self.selection.active:
                # Start selection only once dragging has begun
                self.selection.start = self.camera.window_to_screen(x-dx, y-dy)
            # Update selection
            self.selection.end = self.camera.window_to_screen(x, y)

        if buttons & mouse.MIDDLE:
            self._drag_state[mouse.MIDDLE] = True

        if buttons & mouse.RIGHT:
            self._drag_state[mouse.RIGHT] = True
            self.camera.pan(dx, dy)

    def on_mouse_scroll(self, x: int, y: int, scroll_x: float, scroll_y: float):
        # TODO handle control bindings
        self.camera.zoom(scroll_y * _ZOOM_SCALE_FACTOR)

    def on_key_press(self, symbol, modifiers):
        # TODO handle control bindings

        logger.debug(f"Key press: ({key.symbol_string(symbol)}, {key.modifiers_string(modifiers)})")

        # TODO These controls are useful and should be communicated as control bindings
        if symbol == key.SPACE:
            self.world.paused = not self.world.paused
        elif symbol == key._1:
            self.world.timescale = world.Timescale.SLOW
        elif symbol == key._2:
            self.world.timescale = world.Timescale.MEDIUM
        elif symbol == key._3:
            self.world.timescale = world.Timescale.FAST
        elif symbol == key._4:
            self.world.timescale = world.Timescale.FREERUN
        elif symbol == key.GREATER:
            self.world.active_layer = min(self.world.active_layer + 1, self.world.shape[0] - 1)
        elif symbol == key.LESS:
            self.world.active_layer = max(self.world.active_layer - 1, 0)
        elif symbol == key.PERIOD:
            self.world.step()
            # REMOVEME These controls are for development debug things
        elif symbol == key.GRAVE:
            import bpdb
            bpdb.set_trace()
        elif symbol == key.ESCAPE:
            eventloop.get().exit()
        elif symbol == key.Z:
            if self.world.cursor is not None:
                self.world.set_tile(tiletype=world.tile.TileType.VOID)
                self.add_message(f"Destroyed {self.world.cursor}")
        elif symbol == key.K:
            for e in self.selection.entities:
                if isinstance(e, entities.actors.Creature):
                    e.die()
        elif symbol == key.A:
            if self.world.cursor is not None:
                self.world.atmosphere[(self.world.active_layer, *self.world.cursor)] = 1.0
                self.world._update_atmosphere_colorscale()
        elif symbol == key.D:
            if self.world.cursor is not None:
                # rotate to next designation
                d_values = [d.value for d in world.Designation]
                d = self.world.get_designation().value
                i = (d_values.index(d) + 1) % len(d_values)
                self.world.set_designation(designation=world.Designation(d_values[i]))
        elif symbol == key.S:
            # add a stone
            if self.world.cursor is not None:
                self.world.add_entity(
                    entities.items.Stone(self.world.active_layer, *self.world.cursor)
                )
        elif symbol == key.C:
            # add a cargo pod
            if self.world.cursor is not None:
                self.world.add_entity(
                    entities.items.CargoPod(self.world.active_layer, *self.world.cursor)
                )
        elif symbol == key.P:
            # Make the selected entity go to the nearest item and pick it up, then haul it to the nearest chest, if any.
            def iter_behaviors(creature: entities.actors.Creature) -> T.Iterable[entities.behaviors.Behavior]:
                # find closest item
                item = next(self.world.closest_entities(
                    creature.location,
                    lambda e: isinstance(e, entities.items.Item)
                ), None)

                if item is not None:
                    yield entities.behaviors.MoveToTarget(creature, *item.location)
                    yield entities.behaviors.AddToContainer(creature, item=item, container=creature.inventory)
                    chest = next(self.world.closest_entities(
                        creature.location,
                        lambda e: isinstance(e, entities.items.Chest)
                    ), None)
                    if chest is not None:
                        yield entities.behaviors.MoveToTarget(creature, *chest.location)
                        yield entities.behaviors.TransferItem(creature, item=item, source=creature.inventory, destination=chest)

            for creature in [e for e in self.selection.entities if isinstance(e, entities.actors.Creature)]:
                creature.behavior = entities.behaviors.BehaviorSequence(creature, *iter_behaviors(creature))

    def update(self, dt: float):
        self.world.update(dt)

    def run_forever(self):
        self.window.set_visible()
        eventloop.get().run()
