"""Handling concurrent processing in the pyglet event loop"""
from __future__ import annotations

import typing as T
import concurrent.futures
import logging

import pyglet

logger = logging.getLogger(__name__)

_loop: T.Optional[_ConcurrentEventLoop] = None


class _ConcurrentEventLoop(pyglet.app.EventLoop):
    def __init__(self, *args, executor_kwargs: dict = None, **kwargs):
        super().__init__(*args, **kwargs)
        self._executor = self._init_executor(**(executor_kwargs or {}))
        logger.info(f"Initialized event loop executor as {self._executor}")

    def _init_executor(self) -> concurrent.futures.Executor:
        raise NotImplementedError("Implemented by derived type.")

    def submit(self, fn: T.Callable, *args, **kwargs) -> concurrent.futures.Future:
        return self._executor.submit(fn, *args, **kwargs)

    def exit(self):
        logger.info("Shutting down event loop.")
        self._executor.shutdown(wait=True)
        super().exit()


class ThreadPoolEventLoop(_ConcurrentEventLoop):
    _init_executor = concurrent.futures.ThreadPoolExecutor


def get():
    global _loop
    if _loop is None:
        _loop = ThreadPoolEventLoop()
    return _loop
