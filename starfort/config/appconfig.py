"""Application-wide runtime configuration."""
from dataclasses import dataclass


@dataclass
class AppConfig:
    """Container for application configuration options."""
    fullscreen: bool = False
    window_width: int = 800
    window_height: int = 600
    target_fps: float = 60.0
    vsync: bool = True
    show_fps: bool = False
    debug: bool = False
