"""Starfort (name TBD) -- A space-station management simulator"""

from importlib.metadata import version, PackageNotFoundError
try:
    __version__ = version(__name__)
except PackageNotFoundError:
    __version__ = "develop"

from .application import Application


__all__ = [
    '__version__',
    'Application',
]
