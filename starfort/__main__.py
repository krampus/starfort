"""Application runtime entrypoint"""

import logging

try:
    from colors import color
except ImportError:
    # if colors aren't available, don't bother
    def color(s: str, **kwargs):
        return s

from .config.appconfig import AppConfig


def play(args):
    print("Beginning main runtime")
    from . import application

    config = AppConfig(
        fullscreen=args.fullscreen,
        window_width=args.window_width,
        window_height=args.window_height,
        target_fps=args.target_fps,
        vsync=not args.no_vsync,
        show_fps=args.show_fps,
        debug=args.debug
    )

    try:
        application = application.Application(config=config)
        application.run_forever()
    except Exception as e:
        if config.debug:
            print(e)
            import bpdb
            bpdb.post_mortem()
        else:
            raise e


def main():
    import argparse
    from . import __doc__ as description

    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="log more information. Stacks up to 4 times for increased verbosity.")

    appconfig = parser.add_argument_group('application config',
                                          description="Application-wide runtime configuration.")
    appconfig.add_argument('-F', '--fullscreen', action='store_true',
                           help="run in fullscreen mode.")
    appconfig.add_argument('-W', '--window-width', type=int, default=AppConfig.window_width,
                           help="initial width of the game window, in pixels.")
    appconfig.add_argument('-H', '--window-height', type=int, default=AppConfig.window_height,
                           help="initial height of the game window, in pixels.")
    appconfig.add_argument('--target-fps', type=float, default=AppConfig.target_fps,
                           help="lock framerate to this upper bound.")
    appconfig.add_argument('--no-vsync', action='store_true',
                           help="disable VSYNC.")
    appconfig.add_argument('--show-fps', action='store_true',
                           help="display the framerate.")
    appconfig.add_argument('--debug', action='store_true',
                           help="render debug wireframes.")

    parser.add_argument
    parser.set_defaults(entrypoint=play)
    args = parser.parse_args()

    # configure logging
    log_level = {
        0: logging.CRITICAL,
        1: logging.ERROR,
        2: logging.WARNING,
        3: logging.INFO,
        4: logging.DEBUG,
    }.get(args.verbose, logging.NOTSET)

    fmt_time = color("[%(asctime)s.%(msecs)03d]", fg='green')
    fmt_name = color("[%(name)s]", fg='cyan')
    fmt_source = color("%(module)s.%(funcName)s", fg='yellow') + ':' + color("%(lineno)d", fg='yellow')
    fmt_level = color("%(levelname)s", fg='white')
    logging.basicConfig(
        level=log_level,
        format=f"{fmt_time} {fmt_name} {fmt_source} - {fmt_level} - %(message)s",
        datefmt='%H:%M:%S'
    )

    if args.entrypoint is None:
        parser.print_usage()
    else:
        args.entrypoint(args)


if __name__ == "__main__":
    main()
