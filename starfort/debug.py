"""Debug & development resources and factories."""
from __future__ import annotations

import typing as T
import logging

from . import ui
from .game import world, entities
from .resource import fonts, textures

logger = logging.getLogger(__name__)


def make_world() -> world.World:
    # dim = 10
    # # dim = 50
    # # dim = 100
    # w = world.World((10, dim, dim))

    # # # big block of walls
    # w.tiles[1:-1, 10:-10, 10:-10] = 2

    # # diagonal of floor
    # for i in range(10):
    #     w.tiles[0, i, i] = 1

    # # floor area for entity
    # w.tiles[0, 2:9, 2:9] = 1
    # w.tiles[0, 5, 5] = 2
    # w.tiles[0, 2:9, 1] = w.tiles[0, 2:9, 9] = w.tiles[0, 1, 2:9] = w.tiles[0, 9, 2:9] = 2
    # w.tiles[0, 5, 8] = 4
    # w.tiles[0, 2, 5] = 5
    # w.tiles[0, 5, 2] = 6
    # w.tiles[0, 8, 5] = 7
    # # for i in range(3, 7):
    # #     for j in range(2, 7):
    # #         w.tiles[0, i, j] = 2
    # w.tiles[0, 3, 0] = 1

    # w = world.World((40, 500, 500))

    # return world.World((4, 5, 6))

    w = world.World((10, 50, 50))

    # make a little box to hold an atmosphere
    # ceiling
    w.tiles[1, 5:15, 5:15] = w.tiles[4, 5:15, 5:15] = world.tile.TileType.WALL.value
    # walls
    w.tiles[1:5, 5:15, 5] = w.tiles[1:5, 5:15, 14] = \
        w.tiles[1:5, 5, 5:15] = w.tiles[1:5, 14, 5:15] = world.tile.TileType.WALL.value

    # make a little zone with some things...
    w.tiles[2, 6:14, (6, 13)] = w.tiles[2, (6, 13), 6:14] = world.tile.TileType.WALL.value
    w.tiles[2, 7, 10] = world.tile.TileType.STAIRS_NW.value
    w.tiles[2, 9, 12] = world.tile.TileType.STAIRS_NE.value
    w.tiles[2, 9, 7] = world.tile.TileType.STAIRS_SW.value
    w.tiles[2, 12, 10] = world.tile.TileType.STAIRS_SE.value
    w.tiles[2, 9, 10] = world.tile.TileType.WALL.value

    # # pressurize exactly half of our little box
    # w.atmosphere[2:4, 5:15, 5:10] = 1.0

    # pressurize the whole box
    w.atmosphere[2:4, 6:14, 6:14] = 1.0

    w._update_atmosphere_colorscale()

    return w


def _ent_icon(etype: T.Type[entities.Entity]):
    return ui.widget.Image(textures.ENTITYSHEET[etype.texture_index], width=20, height=20)


def make_world_menu(w: world.World, location: T.Tuple[int, int, int]) -> ui.menu.Menu:
    def tile_setter(ttype: world.tile.TileType):
        return lambda: w.set_tile(*location, tiletype=ttype)

    def designation_setter(dtype: world.Designation):
        return lambda: w.set_designation(*location, designation=dtype)

    spawnable = [entities.actors.Mite,
                 entities.actors.Pawn,
                 entities.items.Stone,
                 entities.items.Ore,
                 entities.items.Steel,
                 entities.items.CargoPod]

    def spawner(etype: T.Type[entities.Entity]):
        return lambda: w.add_entity(etype(*location))

    ttype = w.get_tile(*location)
    menu = ui.menu.Menu(
        (f"{ttype}", None),
        ("destroy tile", lambda: w.destroy_tile(*location)),
        ("set tile...", (
            (t.name, tile_setter(t)) for t in world.tile.TileType
        )),
        ("set designation...", (
            (d.name, designation_setter(d)) for d in world.Designation
        )),
        ("spawn...", (
            (_ent_icon(e), e.__name__, spawner(e)) for e in spawnable
        ))
    )

    return menu


def make_entity_menu(e: entities.Entity) -> ui.menu.Menu:
    listeners = []
    menu = ui.menu.Menu(*_iter_entity_menu(e, listeners))
    for listener in listeners:
        menu.register_handlers(e, **listener)
    # TODO resource leak here
    e.push_handlers(on_remove=lambda e: menu.close())
    return menu


def _iter_entity_menu(e: entities.Entity,
                      listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    yield (_ent_icon(e), f"{e.__class__.__name__}: {e.display_name}", None)

    location_label = ui.menu.make_menu_item_label()

    def on_move(*args):
        location_label.text = f"location: {e.location}"

    on_move()
    listeners.append(dict(on_move=on_move))

    yield (location_label, None)

    if isinstance(e, entities.actors.Creature):
        yield from _iter_creature_menu(e, listeners)
    if isinstance(e, entities.actors.Actor):
        yield from _iter_actor_menu(e, listeners)
    if isinstance(e, entities.actors.Pawn):
        yield from _iter_pawn_menu(e, listeners)
    if isinstance(e, entities.items.Item):
        yield from _iter_item_menu(e, listeners)
    if isinstance(e, entities.items.Container):
        yield from _iter_container_menu(e, listeners)


def _iter_actor_menu(e: entities.actors.Actor,
                     listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    behavior_label = ui.menu.make_menu_item_label()
    behavior_display = ui.menu.make_menu_item_label()

    def on_set_behavior(*args):
        behavior_label.text = f"behavior: {e.behavior.__class__.__name__}"
        behavior_display.text = e.behavior.display_noun

    on_set_behavior()
    listeners.append(dict(on_set_behavior=on_set_behavior))

    def setter_for(behavior: entities.behaviors.Behavior) -> T.Callable:
        def setter():
            e.behavior = behavior
        return setter

    def iter_behaviors() -> T.Iterable[ui.menu._DeclarativeMenu]:
        yield entities.behaviors.StandStill(e)
        yield entities.behaviors.Wander(e)
        if e.world.cursor is not None:
            location = (e.world.active_layer, *e.world.cursor)
            yield entities.behaviors.Move(e, *location)
            yield entities.behaviors.MoveToTarget(e, *location)

    yield (behavior_label, None)
    yield (behavior_display, None)

    yield ("set behavior...", (
        (b.display_imperative, setter_for(b)) for b in iter_behaviors()
    ))


def _iter_creature_menu(e: entities.actors.Creature,
                        listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    # TODO what else?
    yield from _iter_container_menu(e.inventory, listeners)


def _iter_item_menu(e: entities.items.Item,
                    listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    yield (e.__doc__, None)
    yield (f"mass: {e.mass} kg", None)
    yield (f"size: {e.size} units", None)


def _iter_pawn_menu(e: entities.actors.Pawn,
                    listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    pass  # TODO


def _iter_container_menu(c: entities.items.Container,
                         listeners: T.List[T.Mapping[str, T.Callable]]) -> T.Iterable[ui.menu._DeclarativeMenu]:
    def iter_container_submenu() -> T.Iterable[ui.menu._DeclarativeMenu]:
        # TODO update dynamically
        ml, sl, ql = c.mass_limit, c.size_limit, c.quantity_limit
        if all(limit is None for limit in (ml, sl, ql)):
            yield ("(unlimited capacity)", None)
        else:
            if ml is not None:
                yield (f"mass: {c.contained_mass} / {ml} kg", None)
            if sl is not None:
                yield (f"size: {c.contained_size} / {sl} units", None)
            if ql is not None:
                yield (f"capacity: {len(c)} / {ql} items", None)

        for item in c:
            yield (_ent_icon(item), item.display_name, _iter_item_menu(item, listeners))

    yield ("inventory...", iter_container_submenu())
