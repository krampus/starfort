"""Starfort-defined exceptions."""


class StarfortException(Exception):
    """Base class for all Starfort-specific exceptions."""
    pass


class GameLogicException(StarfortException):
    """Exceptions caused by failures in game logic."""
    pass


class UIException(StarfortException):
    """Exceptions caused by failures in UI logic."""
    pass
